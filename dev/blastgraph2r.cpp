//compile: g++ blastgraph2r.cpp -o blastgraph2r
//run: ./blastgraph2r BLASTGRAPH1 BLASTGRAPH2 BLASTGRAPH3 ...

// reduces the edgelist to edgeIDs (so each unqiue edge gets identified with a number 1...m and only the IDs are printed)

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <string.h>

using namespace std;

// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {

	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

}

pair<string,string> orderedStringPair(string a, string b){if(a<b)return make_pair(a,b);else return make_pair(b,a);}

map<string,unsigned int> reduce;
unsigned int c=0;

int main(int argc, char *argv[]) {

	string line;
    unsigned int argi;

	for(argi = 1 ; argi < argc ; argi++){
		if(argv[argi] == string("--help") || argv[argi] == string("-h")){
			PRINTHELP:
			cout << "USAGE:" << endl << "blastgraph2r blastpBLASTGRAPH otherBLASTGRAPH1 (otherBLASTGRAPH2 otherBLASTGRAPH3 ...) " << endl << "produces a r readable file"<< endl;
			return 0;
		}
	}
	string id_a="",id_b="";
	try{

		for(argi = 1 ; argi < argc ; argi++){
			if(strlen(argv[argi])==0 || argv[argi][0] == '-'){continue;}

			string cur_file=argv[argi];
			cur_file+=".r";

			ofstream graph_fileOUT( cur_file.c_str() );

			ifstream graph_file(argv[argi]);

			if (graph_file.is_open()) {

				while (!graph_file.eof()) {

					getline(graph_file, line);

					vector<string> fields;
					tokenize(line, fields, "\t");

					if (line.length()>1 && line.substr(0, 1) == "#"&& fields.size()==2) {
						id_a=fields[0].substr(2, fields[0].size()-2);
						id_b=fields[1];
					}
					if (line.length()>1 && line.substr(0, 1) != "#") {

						if(fields.size()<2)
							continue;

						if(reduce.count(id_a+fields[0]+"-"+id_b+fields[1])){
							graph_fileOUT << reduce[id_a+fields[0]+"-"+id_b+fields[1]] << endl;
						}else{
							c++;
							reduce[id_a+fields[0]+"-"+id_b+fields[1]]=c;
							graph_fileOUT << c << endl;
						}

						// cout <<fields[0]<<"-"<<fields[1] << endl;

					}
				}
			}

			graph_fileOUT.close();

		}
	
	}catch(string& error) {
		cerr << "[ERROR] catched " << error << endl;
		goto PRINTHELP;
		return EXIT_FAILURE;
	}
	return 0;
}
