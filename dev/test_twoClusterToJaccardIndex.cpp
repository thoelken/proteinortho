//pk

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <algorithm>
#include <cmath>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdlib>
#include <ctime>

#include <omp.h>

using namespace std;

struct wedge {unsigned int edge; unsigned short weight;};
struct protein {vector<wedge> edges; unsigned int species_id; string full_name;};
vector<protein> graph;			// Graph containing all protein data
vector<string> species;			// Number -> Name

// Functions
double string2double(string);
void tokenize(const string& , vector<string>& , const string&);
void parse_file(string);
void parse_removegraph(string);
void remove_edge_index(const unsigned int, const unsigned int);
void partition_graph(map<string,bool> * cluster);

// TMP Globals
map<string,int> species2id;		// Name -> Number
map<string,int> protein2id;		// Name -> Number
unsigned int edges = 0;			// number of edges
unsigned int species_counter = 0;	// Species
unsigned int protein_counter = 0;	// Proteins


///////////////////////////////////////////////////////////
// Main
///////////////////////////////////////////////////////////
void clear_edges(vector<unsigned int>& nodes) {
	for (unsigned int i = 0; i < nodes.size(); i++) graph[nodes[i]].edges.clear();
}


int main(int argc, char *argv[]) {

	try {
		
		cerr << "USAGE : ./test_twoClusterToJaccardIndex GRAPHFILE1 GRAPHFILE2" << endl;

		vector<string> files;
		for (int paras = 1; paras < argc; paras++) {
			string parameter = string(argv[paras]);
			if (parameter.substr(0, 1) != "-") {
				files.push_back(parameter);
			}
		}

		cerr<<"-----" << files[1]<<"-----" << endl;

		parse_file(files[0]);

		cerr << species_counter << " species" << endl << protein_counter << " paired proteins" << endl << edges << " bidirectional edges" << endl;
		// No more preallocated memory required
		//graph.shrink_to_fit();

		parse_removegraph(files[1]);

		cerr << species_counter << " species" << endl << protein_counter << " paired proteins" << endl << edges << " bidirectional edges" << endl;

		map<string,bool> cluster_A;
		partition_graph(&cluster_A);

		//clear all

		graph.clear();
		species.clear();
		species_counter = 0;
		protein_counter = 0;
		edges=0;
		species2id.clear();
		protein2id.clear();

		cerr <<"-----"<<files[2]<<"-----" << endl;


		parse_file(files[0]);

		cerr << species_counter << " species" << endl << protein_counter << " paired proteins" << endl << edges << " bidirectional edges" << endl;
		// No more preallocated memory required
		//graph.shrink_to_fit();

		parse_removegraph(files[2]);

		cerr << species_counter << " species" << endl << protein_counter << " paired proteins" << endl << edges << " bidirectional edges" << endl;

		map<string,bool> cluster_B;
		partition_graph(&cluster_B); 

		int n00 = 0, n01 = 0, n10 = 0, n11 = 0;

		for (map<string,bool>::iterator it = cluster_A.begin() ; it != cluster_A.end() ; ++it){
			//cerr << (*it).first << endl;
			if(cluster_B.count((*it).first)){
				n11++;
			}else{
				n10++;
			}
		}
		cerr << endl;
		for (map<string,bool>::iterator it = cluster_B.begin() ; it != cluster_B.end() ; ++it){
			//cerr << (*it).first << endl;
			if(!cluster_A.count((*it).first)){
				n01++;
			}
		}

		cerr <<"n11="<<n11 << " n01=" << n01 << " n10=" << n10 << " n11/(n01+n10+n11)=" << (double)n11/(double)(n01+n10+n11) << endl;

		{
			/* code */
		}

	}
	catch(string& error) {
		cerr << "[ERROR]   " << error << endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

// Remove edge in a single direction given an index -- openMP safe
void remove_edge_index(const unsigned int node_id, const unsigned int index) {
	protein node = graph[node_id];
	if (index >= graph[node_id].edges.size()) {
		stringstream ss;
		ss << "Called index out of bounds: " << node.full_name << " sized " << graph[node_id].edges.size() << " called " << index;
		throw ss.str();
	}

	// log
//	cerr << "Removed " << node.full_name << " -> " << graph[node.edges[index].edge.edge].full_name << " (weight: " << node.edges[index].edge.weight << ")" << endl;
//	#pragma omp critical
	//graph_clean << node.full_name << "\t" << species[node.species_id] << "\t" << graph[node.edges[index].edge].full_name << "\t" << species[graph[node.edges[index].edge].species_id] << endl;

//	print_edgelist(node, index, node_id);

	// overwrite this position with the last
	graph[node_id].edges[index] = graph[node_id].edges.back();
	// and remove last (also works if index == last)
	graph[node_id].edges.pop_back();

//	node = graph[node_id];
//	print_edgelist(node, index, node_id);
}
void partition_graph(map<string,bool> * cluster) {

	vector<bool> done = vector<bool> (protein_counter, false);	// Keep track on what was done

	// For each protein (no increment here, we might redo protein i)
	for (unsigned int protein_id = 0; protein_id < graph.size();) {
//		cerr << "Protein ID: " << protein_id << endl;
		// We were here already
		if (done[protein_id])		{protein_id++;	continue;}

		// Mark protein as done
		done[protein_id] = true;

		// Init todo list with this protein
		stack<unsigned int> todo;				// Coloring stack
		todo.push(protein_id);

		// Collect members of the current connected component
		vector<unsigned int> current_group;			// Keep track of group
		while (!todo.empty()) {
			// Get next protein & remove it from stack
			unsigned int next = todo.top();
			todo.pop();
			// Add protein to current group
			current_group.push_back(next);

			// Add targets to todo list
			for (unsigned int i = 0; i < graph[next].edges.size(); i++) {
				unsigned int target = graph[next].edges[i].edge;
				// Mark & add if unknown yet
				if (!done[target]) {
					done[target] = true;
					todo.push(target);
				}
			} // For each target
		} // While todo list is not empty

		// Do not report singles
		if (current_group.size() < 2) {
			protein_id++;
			continue;
		}

		//cerr << protein_id << " " << protein_counter << endl;
		string id="";
		std::vector<string> vec;
		for (unsigned int i = 0; i < current_group.size(); i++) {
			vec.push_back(graph[current_group[i]].full_name);
			
		}
		sort(vec.begin(), vec.end());
		for (unsigned int i = 0; i < current_group.size(); i++) {
			id+=vec[i]+", ";
		}
		(*cluster)[id] = true;
		//cerr <<id<< endl;
		
		// Clean up and go on with the next connected component
		clear_edges(current_group);
		protein_id++;
	} // For each protein (mainloop)
}
///////////////////////////////////////////////////////////
// File parser
///////////////////////////////////////////////////////////
void parse_file(string file) {
	string line;
	ifstream graph_file(file.c_str());
	if (graph_file.is_open()) {
		// For each line
		string file_a = "";	string file_a_id = "";
		string file_b = "";	string file_b_id = "";
		while (!graph_file.eof()) {
			getline(graph_file, line);
			vector<string> fields;
			tokenize(line, fields, "\t");
			// Header line
			if (fields.size() == 2 && fields[0].substr(0, 1) == "#") {
				file_a = fields[0].substr(2, fields[0].size()-2);
				file_b = fields[1];

				if (file_a == "file_a" && file_b == "file_b") continue;	// Init Header

				// Map species a
				if (species2id.find(file_a) == species2id.end())	{
						species.push_back(file_a);
//						cerr << "Species " << species_counter << ": " << file_a << endl;
						species2id[file_a] = species_counter++;
				}
				// Map species b
				if (species2id.find(file_b) == species2id.end())	{
//						cerr << "Species " << species_counter << ": " << file_b << endl;
						species.push_back(file_b);
						species2id[file_b] = species_counter++;
				}

				file_a_id = file_a;//species2id[file_a];
				file_b_id = file_b;//species2id[file_b];
			}
			// Data line
			else if ((fields.size() == 6 || fields.size() == 8) && fields[0].substr(0, 1) != "#") {
				// a b e1 b1 e2 b2 score

//				cerr << protein_counter << ": " << fields[0] << " <-> " << fields[1] << endl;

				// 5.16 deal with duplicated IDs by adding file ID to protein ID
				string ida = fields[0];
				string idb = fields[1];
				fields[0] += " "; fields[0] += file_a_id;
				fields[1] += " "; fields[1] += file_b_id;

				// 5.16 do not point to yourself
				if (!fields[0].compare(fields[1])) {continue;}

				// A new protein
				if (protein2id.find(fields[0]) == protein2id.end())	{
					protein a;
					a.full_name	= ida;
					a.species_id	= species2id[file_a_id];
					protein2id[fields[0]] = protein_counter++;
					graph.push_back(a);
				}
				if (protein2id.find(fields[1]) == protein2id.end())	{
					protein b;
					b.full_name	= idb;
					b.species_id	= species2id[file_b_id];
					protein2id[fields[1]] = protein_counter++;
					graph.push_back(b);
				}

				// Bitscores
				unsigned short bitscore_a = string2double(fields[3]);
				unsigned short bitscore_b = string2double(fields[5]);
				unsigned short bitscore_avg = (bitscore_a+bitscore_b)/2;
		
				// Add link to graph (reciprocal)					
				unsigned int a_id = protein2id[fields[0]];
				unsigned int b_id = protein2id[fields[1]];

				// 5.17, add weight
				wedge w;
				w.edge=b_id;
				w.weight=bitscore_avg;
				graph[a_id].edges.push_back(w);
				w.edge=a_id;
				w.weight=bitscore_avg;
				graph[b_id].edges.push_back(w);
				edges++;
			}
		}
	}
	else {
		throw string("Could not open file " + file);
	}
}
map<string,int> keys;
void parse_removegraph(string file) {
	string line;
	ifstream graph_file(file.c_str());
	if (graph_file.is_open()) {
		// For each line
		while (!graph_file.eof()) {
			getline(graph_file, line);
			vector<string> fields;
			tokenize(line, fields, "\t");
			// Header line
			if (fields.size() == 4 ) {
				// a b e1 b1 e2 b2 score

//				cerr << protein_counter << ": " << fields[0] << " <-> " << fields[1] << endl;

				// 5.16 deal with duplicated IDs by adding file ID to protein ID
				fields[0] += " "; fields[0] += fields[1];
				fields[2] += " "; fields[2] += fields[3];

				// Add link to graph (reciprocal)					
				unsigned int a_id = protein2id[fields[0]];
				int b_id = -1;
				for(int i = 0 ; i < graph[a_id].edges.size() ; i++){
					if(graph[a_id].edges[i].edge == protein2id[fields[2]]){
						b_id = i;
						break;
					}
				}

				bool removeline = false;

				if(b_id>=0){
					removeline = true;
					remove_edge_index(a_id,b_id);
				}



				a_id = protein2id[fields[2]];
				b_id = -1;
				for(int i = 0 ; i < graph[a_id].edges.size() ; i++){
					if(graph[a_id].edges[i].edge == protein2id[fields[0]]){
						b_id = i;
						break;
					}
				}
				if(b_id>=0){
					removeline = true;
					remove_edge_index(a_id,b_id);
				}

				if(removeline)
					edges--;
				//cerr << edges << endl;
			}
		}
	}
	else {
		throw string("Could not open file " + file);
	}
}

// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {
	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}
}
double string2double(string str) {
	istringstream buffer(str);
	double value;
	buffer >> value;
	return value;
}