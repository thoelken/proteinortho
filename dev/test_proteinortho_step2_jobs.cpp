//compile: g++ test_proteinortho_step2_jobs.cpp -o test_proteinortho_step2_jobs 
//run: ./test_proteinortho_step2_jobs fasta1 fasta2 ... fastaN BLASTGRAPH
//pk
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>
#include <algorithm>
#include <cmath>

using namespace std;

// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {

	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

}

pair<string,string> orderedStringPair(string a, string b){if(a<b)return make_pair(a,b);else return make_pair(b,a);}

map<pair<string,string>,unsigned int > pairwiseFileNames;
std::vector<pair<string,string> > notInBlastFileButInFastaDir, multipleOccInBlastFile;

int main(int argc, char *argv[]) {

	for(int argi = 1 ; argi < argc-1 ; argi++){
		string f1 = argv[argi];
		f1=f1.substr(f1.find_last_of("/")+1);
		for(int argj = argi+1 ; argj < argc-1 ; argj++){
			string f2 = argv[argj];
			f2=f2.substr(f2.find_last_of("/")+1);
			pairwiseFileNames[orderedStringPair(f1,f2)]=0;
		}
	}

	int argi = argc-1;

	string line;
	ifstream graph_file(argv[argi]);

	int isfirstline=1;

	pair<string,string> lastFilePair = make_pair("","");

	if (graph_file.is_open()) {

		while (!graph_file.eof()) {

			getline(graph_file, line);

			if(isfirstline>0){

				isfirstline--;

			}else{

				if(line.size()<1) continue;

				vector<string> fields;
				tokenize(line, fields, "\t");

				if(fields.size()<2) continue;

				if (fields.size() == 2 && fields[0].substr(0, 1) == "#") {
					// Header line
					fields[0] = fields[0].substr(1, fields[0].size() - 1);
					if(fields[0].substr(0, 1) == " "){
						fields[0] = fields[0].substr(1, fields[0].size() - 1);
					}

					if(!pairwiseFileNames.count(orderedStringPair(fields[0],fields[1]))){
						notInBlastFileButInFastaDir.push_back(orderedStringPair(fields[0],fields[1]));
					}else{
						if(pairwiseFileNames[orderedStringPair(fields[0],fields[1])]==1){
							multipleOccInBlastFile.push_back(orderedStringPair(fields[0],fields[1]));
						}
						pairwiseFileNames[orderedStringPair(fields[0],fields[1])]=1;
						lastFilePair=orderedStringPair(fields[0],fields[1]);
					}
				}else{
					pairwiseFileNames[lastFilePair]++;
				}
			}
		}
	}

	int numErrs = 0 ;
	// 1. catch the cases of pairs of files in fasta dir that are not in the blast output
	for(unsigned int i = 0 ; i < notInBlastFileButInFastaDir.size() ; i++){
		cout << "ERROR type A1 found \"" <<  notInBlastFileButInFastaDir[i].first << " "<< notInBlastFileButInFastaDir[i].second << "\" in blast file but not in fasta dir !!!" << endl;
		numErrs ++;
	}
	// 2. catch the cases of reoccuring pairs of files in blast file (multiple entries are not neccessarily bad)
	for(unsigned int i = 0 ; i < multipleOccInBlastFile.size() ; i++){
		cout << "WARNING type A2 found \"" <<  multipleOccInBlastFile[i].first << " "<< multipleOccInBlastFile[i].second << "\" multiple times in blast file !!!" << endl;
	}
	for(map<pair<string,string>,unsigned int >::iterator it = pairwiseFileNames.begin() ; it != pairwiseFileNames.end() ; ++it){
		
		if((*it).first.first.size()==0 || (*it).first.second.size()==0) continue;  

		if((*it).second==0){

			// 0 = found a pair of fasta files that are not found in the blast output file -> ERROR

			cout << "ERROR type B found \"" << (*it).first.first << " "<< (*it).first.second << "\" in fasta dir but not in blast file !!!" << endl;
			numErrs ++;
		}else if((*it).second==1){

			// 1 = the pair is found in blast and fasta + but no lines (blast scores etc...) afterwards -> only the # FILEA FILEB is present 
			// must not be an error, if no rbh is found between the two files 

			cout << "WARNING type C found \"" << (*it).first.first << " "<< (*it).first.second << "\" in fasta dir but empty in blast file !!!" << endl;
		}
		// else -> pair is found in blast file and fasta + more then 1 line aftwards (rbh match of blast) -> OK FINE 

	}

	if(numErrs==0){cout << "no errors found, all ok" << endl;}
	
	return 0;
}
