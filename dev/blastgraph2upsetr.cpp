//compile: g++ blastgraph2upsetr.cpp -o blastgraph2upsetr 
//run: ./blastgraph2upsetr BLASTGRAPH1 BLASTGRAPH2 ... 
// "produces a table in the form:" << endl;
// "edgeID	isInFirstFile	isInSecondFile	..." << endl;
// "1	0	1	..." << endl;
// "2	0	0	..." << endl;
// "3	1	1	..." << endl;
// "..." << endl;
// "Output is written STDOUT" << endl;
//pk
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <string.h>

using namespace std;

// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {

	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

}

string orderedStringPairID(string a, string b){if(a<b)return a+"-"+b;else return b+"-"+a;}

map<string,unsigned int> reduce;
vector<vector<bool> > is_in_file;

int main(int argc, char *argv[]) {

	string id_a="",id_b="";
	//ofstream graph_fileOUT( "myiven.upsetr" );

	bool idBetweenBars=false; 
	unsigned int argi;
	for(argi = 1 ; argi < argc ; argi++){
		if(argc < 2 || argv[argi] == "-h" || argv[argi] == "--h" || argv[argi] == "-help" || argv[argi] == "--help" || argv[argi] == "help" || argv[argi] == "h"){
			PRINTHELP:
			cerr << "./blastgraph2upsetr BLASTGRAPH1 BLASTGRAPH2 ..." << endl;
			cerr << "produces a table in the form:" << endl;
			cerr << "edgeID	isInFirstFile	isInSecondFile	..." << endl;
			cerr << "1	0	1	..." << endl;
			cerr << "2	0	0	..." << endl;
			cerr << "3	1	1	..." << endl;
			cerr << "..." << endl;
			cerr << "Output is written STDOUT" << endl;
			return -1;
		}else if(argv[argi] == string("--idBetweenBars") || argv[argi] == string("-idBetweenBars") || argv[argi] == string("-bar") || argv[argi] == string("-bars")){
			idBetweenBars=true; 
			continue;
		}else if(strlen(argv[argi])==0 || argv[argi][0] == '-'){
			cerr << "[ERROR] : unknown option" << endl;
			goto PRINTHELP;
		}
	}
	if(argc < 2){
		goto PRINTHELP;
	}

	try{
		cout << "edgeID";
		for(argi = 1 ; argi < argc ; argi++){
			if(strlen(argv[argi])==0 || argv[argi][0] == '-'){continue;}
			cout << "\t" << argv[argi];
		}
		cout << endl;

		unsigned int numberofinputsets=0;

		for(argi = 1 ; argi < argc ; argi++){
			if(strlen(argv[argi])==0 ||argv[argi][0] == '-'){continue;}
			numberofinputsets++;
		}
		string cur_species_id = "";

		for(argi = 1 ; argi < argc ; argi++){
			if(strlen(argv[argi])==0 ||argv[argi][0] == '-'){continue;}

			string line;
			ifstream graph_file(argv[argi]);

			if (graph_file.is_open()) {
				while (!graph_file.eof()) {
					getline(graph_file, line);

					vector<string> fields;
					tokenize(line, fields, "\t");

					if (line.length()>1 && line.substr(0, 1) == "#"&& fields.size()==2) {
						id_a=fields[0].substr(2, fields[0].size()-2);
						id_b=fields[1];

						cur_species_id = orderedStringPairID(id_a,id_b);
					}
					if (line.length()>1 && line.substr(0, 1) != "#") {

						vector<string> fields2;
						if(fields.size()>1 && idBetweenBars){
							tokenize(fields[0], fields2, "|");
							if(fields2.size()==3){fields[0]=fields2[1];}
							fields2.clear();
							tokenize(fields[1], fields2, "|");
							if(fields2.size()==3){fields[1]=fields2[1];}
						}

						if(fields.size()<2)
							continue;

						//cerr << id_a+fields[0] << id_b+fields[1] << endl;

						string cur_id = cur_species_id+orderedStringPairID(fields[0],fields[1]);

						//cerr << cur_id << endl;

						if(reduce.count(cur_id)){
							is_in_file[reduce[cur_id]][argi-(argc-numberofinputsets)]=true;
						}else{
							reduce[cur_id]=is_in_file.size();
							vector<bool> cur_bit(numberofinputsets,false);
							cur_bit[argi-(argc-numberofinputsets)]=true;
							is_in_file.push_back(cur_bit);
						}
					}
				}
			}
		}

		for(unsigned int i = 0 ; i < is_in_file.size() ; i++){
			cout << i << "\t";
			for(unsigned int j = 0 ; j < is_in_file[i].size() ; j++){
				cout << is_in_file[i][j];
				if(j != is_in_file[i].size()-1)
					cout << "\t";
			}
			cout << endl;
		}

	}catch(string& error) {
		cerr << "[ERROR] catched " << error << endl;
		goto PRINTHELP;
		return EXIT_FAILURE;
	}
	
	return 0;
}
