//compile: g++ blastgraph2diffBitscoresEvalues.cpp -o blastgraph2diffBitscoresEvalues 
//run: ./blastgraph2diffBitscoresEvalues blastpBLASTGRAPH otherBLASTGRAPH(e.g.diamond)
//pk
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <string.h>

using namespace std;

// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {

	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

}

double string2double(string str) {
	istringstream buffer(str);
	double value;
	buffer >> value;
	return value;
}

pair<string,string> orderedStringPair(string a, string b){if(a<b)return make_pair(a,b);else return make_pair(b,a);}

map<string,pair<double,double> > dataFirst;
map<string,bool > isInSecond;

int main(int argc, char *argv[]) {

	if(argc != 3 || argv[1] == "-h" || argv[1] == "--h" || argv[1] == "-help" || argv[1] == "--help" || argv[1] == "help" || argv[1] == "h"){
		PRINTHELP:
		cout << "USAGE: blastgraph2diffBitscoresEvalues blastBLASTGRAPH otherBLASTGRAPH" << endl << "couts a table with all identifier (pairs/edges of genes) and the difference in bitscore and evalue as BLAST-OTHER" << endl<< "e.g. P1-P2	-.3	-100" << endl << "= blast has a .3 smaller evalue of P1-P2 match or an bitscore that is 100 points smaller." << endl;
		return -1;
	}else if(strlen(argv[1])==0 || argv[1][0] == '-'){
		cout << "[ERROR] : unknown option" << endl;
		goto PRINTHELP;
	}

	try {
			
		string line;
		ifstream graph_file(argv[1]);

		if (graph_file.is_open()) {
			while (!graph_file.eof()) {
				getline(graph_file, line);

				if (line.length()>1 && line.substr(0, 1) != "#") {

					vector<string> fields;
					tokenize(line, fields, "\t");

					if(fields.size()<2)
						continue;

					dataFirst[fields[0]+"-"+fields[1]]=make_pair( (string2double(fields[2])+string2double(fields[4]))/2 , (string2double(fields[3])+string2double(fields[5]))/2 );

				}
			}
		}

		graph_file.close();
		graph_file.open(argv[2]);

		if (graph_file.is_open()) {
			while (!graph_file.eof()) {
				getline(graph_file, line);

				if (line.length()>1 && line.substr(0, 1) != "#") {

					vector<string> fields;
					tokenize(line, fields, "\t");

					if(isInSecond.count(fields[0]+"-"+fields[1])){
						cerr << "ERROR 1 : found duplicates ..." << endl;
						return -1;
					}
					if(fields.size()<2){
						continue;
					}

					isInSecond[fields[0]+"-"+fields[1]]=1;
					dataFirst[fields[0]+"-"+fields[1]] = make_pair( 
						dataFirst[fields[0]+"-"+fields[1]].first - (string2double(fields[2])+string2double(fields[4]))/2 , 
						dataFirst[fields[0]+"-"+fields[1]].second - (string2double(fields[3])+string2double(fields[5]))/2 );
				}
			}
		}

		for(map<string,pair<double,double> >::iterator it = dataFirst.begin(); it != dataFirst.end() ; ++it){
			if(isInSecond.count((*it).first)){
				cout << (*it).first << "," ;
				cout << scientific << (*it).second.first << ",";
				cout << fixed << (*it).second.second << endl;

			}
		}

	}catch(string& error) {
		cerr << "[ERROR] catched " << error << endl;
		goto PRINTHELP;
		return EXIT_FAILURE;
	}
	return 0;
}
