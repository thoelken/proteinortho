//compile: g++ blastgraph2sensitivity.cpp -o blastgraph2sensitivity 
//run: ./blastgraph2sensitivity blastpBLASTGRAPH otherBLASTGRAPH1 otherBLASTGRAPH2 otherBLASTGRAPH3 ... 
//pk
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <string.h>

using namespace std;

// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {

	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

}

pair<string,string> orderedStringPair(string a, string b){if(a<b)return make_pair(a,b);else return make_pair(b,a);}

map<string,bool> dataFirst;

int main(int argc, char *argv[]) {

	string id_a="",id_b="";

	bool idBetweenBars=false;
	bool isFirst=true;
	unsigned int argi;

	for(argi = 1 ; argi < argc ; argi++){ 

		if(argv[argi] == string("--help") || argv[argi] == string("-h")){
			PRINTHELP:
			cout << "USAGE:" << endl << "blastgraph2sensitivity (--idBetweenBars,-bars) blastpBLASTGRAPH otherBLASTGRAPH1 (otherBLASTGRAPH2 otherBLASTGRAPH3 ...) " << endl << "couts the number of lines for each given file and the number of edges that are also present in the first file."<< endl << "--idBetweenBars : if enabled, then ids are converted such that adkjas|132kkm|kmlkq -> 132kkm."<< endl;
			return 0;
		}
		if(argv[argi] == string("--idBetweenBars") || argv[argi] == string("-idBetweenBars") || argv[argi] == string("-bar") || argv[argi] == string("-bars")){
			idBetweenBars=true; 
			continue;
		}else if(strlen(argv[argi])==0 || argv[argi][0] == '-'){
			cout << "[ERROR] : unknown option" << endl;
			goto PRINTHELP;
		}

		try {

			string line;
			ifstream graph_file(argv[argi]);

			unsigned int numOfLines = 0;

			cout << argv[argi] << " " ;

			if(isFirst){
				isFirst=false;

				if (graph_file.is_open()) {
					while (!graph_file.eof()) {
						getline(graph_file, line);

						vector<string> fields;
						tokenize(line, fields, "\t");

						if (line.length()>1 && line.substr(0, 1) == "#" && fields.size()==2) {
							id_a=fields[0].substr(2, fields[0].size()-2);
							id_b=fields[1];
						}
						if (line.length()>1 && line.substr(0, 1) != "#") {
							numOfLines++;

							vector<string> fields2;
							if(fields.size()>1 && idBetweenBars){
								tokenize(fields[0], fields2, "|");
								if(fields2.size()==3){fields[0]=fields2[1];}
								fields2.clear();
								tokenize(fields[1], fields2, "|");
								if(fields2.size()==3){fields[1]=fields2[1];}
							}

							if(fields.size()<2)
								continue;

							string key=""; // the key is a specific mix out of the 2 species names and the 2 protein names
							// the first 2 words are the lexico-ordered species names
							// the next 2 words are the lexico-ordered protein names
							// therefore the any permuation in the order of the species or the proteins result in the same key-id
							if(id_a<id_b){key=id_a+"-"+id_b;}else{key=id_b+"-"+id_a;}
							if(fields[0]<fields[1]){key=key+"--"+fields[0]+"-"+fields[1];}else{key=key+"--"+fields[1]+"-"+fields[0];}

							dataFirst[key]=true; //store all edges from the first file
						}
					}
				}

				cout << numOfLines; //print only the number of lines (since all edges are in the first file, since this is the first file)

			}else{

	  			unsigned int numOfmathes=0;

				if (graph_file.is_open()) {
					while (!graph_file.eof()) {
						getline(graph_file, line);

						vector<string> fields;
						tokenize(line, fields, "\t");

						if (line.length()>1 && line.substr(0, 1) == "#" && fields.size()==2) {
							id_a=fields[0].substr(2, fields[0].size()-2);
							id_b=fields[1];
						}
						if (line.length()>1 && line.substr(0, 1) != "#") {

							numOfLines++;

							if(fields.size()<2)
								continue;

							vector<string> fields2;
							if(fields.size()>1 && idBetweenBars){
								tokenize(fields[0], fields2, "|");
								if(fields2.size()==3){fields[0]=fields2[1];}
								fields2.clear();
								tokenize(fields[1], fields2, "|");
								if(fields2.size()==3){fields[1]=fields2[1];}
							}

							// cout << fields[0]+"-"+fields[1] << " " << (dataFirst.count(fields[0]+"-"+fields[1])) << endl;

							string key="";
							if(id_a<id_b){key=id_a+"-"+id_b;}else{key=id_b+"-"+id_a;}
							if(fields[0]<fields[1]){key=key+"--"+fields[0]+"-"+fields[1];}else{key=key+"--"+fields[1]+"-"+fields[0];}

							if(dataFirst.count(key)) // test if the edge in this current file is part of the first file (is a true positive or not)
								numOfmathes++;
						}
					}
				}

				cout << numOfLines <<" "<< numOfmathes; // print out the number of lines (predicted edges) and the number of true positives (compared to the first file)
			}

			graph_file.close();
			cout << endl;

		}catch(string& error) {
			cerr << "[ERROR] catched " << error << endl;
			goto PRINTHELP;
			return EXIT_FAILURE;
		}
	}

	
	if(argc==1){
		goto PRINTHELP;
	}

	return 0;
}
