// compile g++ generateARandomConnectedBlastGraph.cpp -o generateARandomConnectedBlastGraph
// run : ./generateARandomConnectedBlastGraph nstart nend amount max_d min_d
//pk
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>     /* srand, rand */
#include <map>

using namespace std;

double string2double(string str) {
	istringstream buffer(str);
	double value;
	buffer >> value;
	return value;
}
template<typename T> string num2string(T d) {
	std::ostringstream strs;
	strs << d;
	return strs.str();
}
pair<int,int> orderPair(int a, int b){if(a<b)return make_pair(a,b); else return make_pair(b,a);}

int main(int argc, char *argv[]) {

	srand(213871262);

	if(argc != 4 && argc != 5 && argc != 6){
		PRINTHELP:
		cout << "USAGE:"<< "generateARandomConnectedBlastGraph 10 20 5 max_d min_d" << endl << " -> generates a 5 random graph with 10-20 nodes with a density d such that min_d/1000 <= d < max_d/1000." << endl;
		return -1;
	}
	if(argv[1][0] == '-'){
		cout << "[ERROR] : unknown option, there is no option" << endl;
		goto PRINTHELP;
	}
	if(argv[2][0] == '-'){
		cout << "[ERROR] : unknown option, there is no option" << endl;
		goto PRINTHELP;
	}
	if(argv[3][0] == '-'){
		cout << "[ERROR] : unknown option, there is no option" << endl;
		goto PRINTHELP;
	}

	try{

		int n_start = string2double(argv[1]);
		int n_end = string2double(argv[2]);
		if(n_start == n_end){n_end++;}
		int amount = string2double(argv[3]);
		int max_d=1000;
		int min_d=0;
		if(argc > 4){
			if(argv[4][0] == '-'){
				cout << "[ERROR] : unknown option, there is no option" << endl;
				goto PRINTHELP;
			}
			max_d = string2double(argv[4]);
		}
		if(argc > 5){
			if(argv[5][0] == '-'){
				cout << "[ERROR] : unknown option, there is no option" << endl;
				goto PRINTHELP;
			}
			min_d = string2double(argv[5]);
			if(min_d == max_d){max_d++;}
		}

		for(int amounti = 0 ; amounti < amount ; amounti++){
			
			int n = n_start+rand() % (n_end-n_start);
			double d = ((double) ( ( rand() % (max_d-min_d) ) + min_d ) )/1000.0;
			
			cout <<n << " "<< d << endl;

			if(n-1 > d * ((double)n*(double)(n-1))/2.0) d=(n-1)/((double)n*(double)(n-1))/2.0;

			cout << ("Gn"+num2string<int>(n)+"_d"+num2string<double>(d)+".sav") << endl;

		  	ofstream myfile( ("Gn"+num2string<int>(n)+"_d"+num2string<double>(d)+".sav").c_str() );
			myfile << "# file_a	file_b\n";
			myfile << "# a	b	evalue_ab	bitscore_ab	evalue_ba	bitscore_ba\n";

			map<pair<int,int> , bool > E;


			if(d<0.6){

				for(int i = 0 ; i < n-1 ; i++){
					myfile << i << "	"<<(i+1)<<"	"<<1.0/((double)(rand()%2000))<<"	"<<(rand()%2000)<<"	"<<1.0/((double)(rand()%2000))<<"	"<<(rand()%2000)<<"\n";
					E[orderPair(i,i+1)]=1;
				}

				while( (double)E.size() < d * ((double)n*(double)(n-1))/2.0 ){
					int va=0, vb=0;

					va=rand() % n;
					vb=rand() % n;

					while( va==vb || E.count(orderPair(va,vb))){
						vb=rand() % n;
					}

					myfile << va << "	"<<vb<<"	"<<1.0/((double)(rand()%2000))<<"	"<<(rand()%2000)<<"	"<<1.0/((double)(rand()%2000))<<"	"<<(rand()%2000)<<"\n";

					E[orderPair(va,vb)]=1;

				}
			}else{

				for(unsigned int i = 0 ; i < n ; i++){
					for(unsigned int j = i+1 ; j < n ; j++){
						E[orderPair(i,j)]=1;
					}
				}
				while( (double)E.size() > d * ((double)n*(double)(n-1))/2.0 ){

					int va=0, vb=0;

					va=rand() % n;
					vb=rand() % n;

					while( va==vb || !E.count(orderPair(va,vb)) || va==vb+1 || va==vb-1){
						vb=rand() % n;
					}
					map<pair<int,int>,bool>::iterator it = E.find(orderPair(va,vb));
					E.erase (it);
				}
				for(map<pair<int,int>,bool>::iterator it = E.begin(); it != E.end() ; ++it){
					myfile << (*it).first.first << "	"<<(*it).first.second<<"	"<<1.0/((double)(rand()%2000))<<"	"<<(rand()%2000)<<"	"<<1.0/((double)(rand()%2000))<<"	"<<(rand()%2000)<<"\n";
				}
			}
				
			myfile.close();

		}
		
	}catch(string& error) {
		cerr << "[ERROR] catched " << error << endl;
		goto PRINTHELP;
		return EXIT_FAILURE;
	}
	return 0;
}
