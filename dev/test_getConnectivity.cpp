//compile: g++ -fopenmp -O3 -mtune=native -march=native test_getConnectivity.cpp -o test_getConnectivity -llapack
//run: ./test_getConnectivity 01_USEWEIGHTS 012_modus_0_power_1_dsyevx_2_dsyevr EPSILON CPUTHREADS BLASTGRAPH 

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>
#include <algorithm>
#include <cmath>

using namespace std;

#ifdef _OPENMP
	#include <omp.h>
#endif

bool printEV=false;


extern "C" {
	//dsyevx LAPACK function
	//-      float
	// --    symmetric 
	//   --- eigenvalue expert (more options, e.g. first k eigenvalues...)

	extern void ssyevr_( char* jobz, char* range, char* uplo, int* n, float* a,
                int* lda, float* vl, float* vu, int* il, int* iu, float* abstol,
                int* m, float* w, float* z, int* ldz, int* isuppz, float* work,
                int* lwork, int* iwork, int* liwork, int* info );
	extern void dsyevr_( char* jobz, char* range, char* uplo, int* n, double* a,
                int* lda, double* vl, double* vu, int* il, int* iu, double* abstol,
                int* m, double* w, double* z, int* ldz, int* isuppz, double* work,
                int* lwork, int* iwork, int* liwork, int* info );

}



struct wedge {unsigned int edge; unsigned short weight;};
struct protein {vector<wedge> edges; unsigned int species_id; string full_name;};

// Parameters
bool param_verbose 		= false;
float param_con_threshold 	= 0.1;		// as a reference: a chain a-b-c-d has 0.25
unsigned int debug_level	= 0;
float param_sep_purity 	= 1e-7;		// as a reference: a-b-c will give +/-0.707107 and 2.34857e-08 
unsigned int param_max_nodes	= 32768; //2^15
float param_min_species	= 0;
string param_rmgraph            = "remove.graph";
bool param_useWeights = false;
unsigned int param_minOpenmp = 64;
bool param_useKmereHeuristic = true;
int param_useLapack = 0;

// min/max number of alg con iterations
const unsigned int min_iter = 16;			// below this value, results may vary
unsigned int max_iter = 8192;			// below this value, results may vary
float epsilon = 1e-8; // analog to http://people.sc.fsu.edu/~jburkardt/c_src/power_method/power_method_prb.c
const unsigned int kmereHeuristic_minNodes = 16384; //2^14 
const unsigned int kmereHeuristic_protPerSpecies = 3;
const unsigned int kmereHeuristic_minNumberOfGroups = 3;
// const unsigned int maxUseWeightsNumNodes = 16384; //2^14 

// Globals
unsigned int species_counter = 0;	// Species
unsigned int protein_counter = 0;	// Proteins
vector<string> species;			// Number -> Name
vector<protein> graph;			// Graph containing all protein data
float last_stat = 0;			// For progress stats
unsigned int edges = 0;			// number of edges
ofstream graph_clean;			// File to store graph data
vector<int> reorder_table;		// Tells how proteins/species must be sorted
unsigned long graph_ram_total=0;


// TMP Globals
map<string,int> species2id;		// Name -> Number
map<string,int> protein2id;		// Name -> Number

///////////////////////////////////////////////////////////
// Misc functions
///////////////////////////////////////////////////////////
// Convert string to float
float string2float(string str) {
	istringstream buffer(str);
	float value;
	buffer >> value;
	return value;
}


// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {

	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

}


///////////////////////////////////////////////////////////
// File parser
///////////////////////////////////////////////////////////
void parse_file(string file) {

	// cerr << "Reading " << file << endl;
	string line;
	ifstream graph_file(file.c_str());
	if (graph_file.is_open()) {
		// For each line
		string file_a = "";	unsigned int file_a_id = 0;
		string file_b = "";	unsigned int file_b_id = 0;
		while (!graph_file.eof()) {
			getline(graph_file, line);
			vector<string> fields;
			tokenize(line, fields, "\t");
			// Header line
			if (fields.size() == 2 && fields[0].substr(0, 1) == "#") {
				file_a = fields[0].substr(2, fields[0].size()-2);
				file_b = fields[1];

				if (file_a == "file_a" && file_b == "file_b") continue;	// Init Header

				// Map species a
				if (species2id.find(file_a) == species2id.end())	{
						species.push_back(file_a);
						species2id[file_a] = species_counter++;
				}
				// Map species b
				if (species2id.find(file_b) == species2id.end())	{
						species.push_back(file_b);
						species2id[file_b] = species_counter++;
				}

				file_a_id = species2id[file_a];
				file_b_id = species2id[file_b];
			}
			// Data line
			else if ((fields.size() == 6 || fields.size() == 8) && fields[0].substr(0, 1) != "#") {
				// a b e1 b1 e2 b2 score

				// 5.16 deal with duplicated IDs by adding file ID to protein ID
				string ida = fields[0];
				string idb = fields[1];
				fields[0] += " "; fields[0] += file_a_id;
				fields[1] += " "; fields[1] += file_b_id;

				// 5.16 do not point to yourself
				if (!fields[0].compare(fields[1])) {continue;}

				// A new protein
				if (protein2id.find(fields[0]) == protein2id.end())	{
					protein a;
					a.full_name	= ida;
					a.species_id	= file_a_id;
					protein2id[fields[0]] = protein_counter++;
					graph.push_back(a);
				}
				if (protein2id.find(fields[1]) == protein2id.end())	{
					protein b;
					b.full_name	= idb;
					b.species_id	= file_b_id;
					protein2id[fields[1]] = protein_counter++;
					graph.push_back(b);
				}

				// Bitscores 
				// check range (in float)
				float bit_a = string2float(fields[3]);
				float bit_b = string2float(fields[5]);
				if(bit_a>65535)
					throw string(" ERROR unsigned short overflow " +fields[3] + " (bitscore of "+ida+")");
				if(bit_b>65535)
					throw string(" ERROR unsigned short overflow " +fields[5] + " (bitscore of "+idb+")");

				// assign
				unsigned short bitscore_avg = (bit_a+bit_b)/2;
		
				// Add link to graph (reciprocal)					
				unsigned int a_id = protein2id[fields[0]];
				unsigned int b_id = protein2id[fields[1]];

				// 5.17, add weight
				wedge w;
				w.edge=b_id;
				w.weight=bitscore_avg;
				graph[a_id].edges.push_back(w);
				w.edge=a_id;
				w.weight=bitscore_avg;
				graph[b_id].edges.push_back(w);
				edges++;
			}
		}
		graph_file.close();
		// cerr << "Reading " << file << " DONE "<< endl;
	}
	else {
		throw string("Could not open file " + file);
	}

}

///////////////////////////////////////////////////////////
// Algebraic connectivity functions
///////////////////////////////////////////////////////////
// Return maximum degree of given protein_ids -- openMP A ML
unsigned int max_of_diag(vector<unsigned int>& nodes, vector<unsigned int>& diag) {
	
	unsigned int max = 0;

	bool useOpenMpFlag = (nodes.size() > param_minOpenmp);

	#pragma omp parallel for reduction(max: max) if (useOpenMpFlag)
	for (unsigned int i = 0; i < nodes.size(); i++) {
		if (diag[i] > max) max = diag[i] ;
	}

	return max;
}

// Generate random vector x of size size
vector<float> generate_random_vector(const unsigned int size) {

	vector<float> x(size);

	x[0] = (float)(rand() % 999+1)/1000;	// 0 to 1
	for (unsigned int i = 1; i < size; i++) {
		x[i] = (float)(rand() % 999+1)/1000;	// 0 to 1
		if (x[i] == x[i-1]) x[i] /= 3;		// Check: at least one value must be different from the others but still within 0 and 1
	}

	return x;
}



// determine new X, Formula (1) -- openMP B ML
vector<float> get_new_x(vector<float> x, vector<unsigned int>& nodes, map<unsigned int,unsigned int> &mapping, bool isWeighted) {

	vector<float> x_new(x.size(),0);
	
	bool useOpenMpFlag = (nodes.size() > param_minOpenmp);

	if(isWeighted){

		#pragma omp parallel for schedule(dynamic) if (useOpenMpFlag)
		for (unsigned int i = 0; i < nodes.size(); i++) {

			// go through adjacency list of node 
			for (unsigned int j = 0; j < graph[nodes[i]].edges.size(); j++) {
				// y points to z, so take entry z from x
				unsigned int abs_target = graph[nodes[i]].edges[j].edge;
				unsigned int rel_target = mapping[abs_target];

				x_new[i] += x[rel_target]*(float)graph[nodes[i]].edges[j].weight;
			}
		}

	}else{

		#pragma omp parallel for schedule(dynamic) if (useOpenMpFlag)
		for (unsigned int i = 0; i < nodes.size(); i++) {

			// go through adjacency list of node 
			for (unsigned int j = 0; j < graph[nodes[i]].edges.size(); j++) {
				// y points to z, so take entry z from x
				unsigned int abs_target = graph[nodes[i]].edges[j].edge;
				unsigned int rel_target = mapping[abs_target];

				x_new[i] += x[rel_target];
			}
		}
	}

	return x_new;
}

// Make vector x orthogonal to 1, Formula (2) -- openMP A ML
vector<float> makeOrthogonal(vector<float> x) {

	float sum = 0;

	bool useOpenMpFlag = (x.size() > param_minOpenmp);

	#pragma omp parallel for reduction(+: sum) if (useOpenMpFlag)
	for (unsigned int i = 0; i < x.size(); i++) {sum += x[i];}

	float average = sum/x.size();

	#pragma omp parallel for if (useOpenMpFlag)
	for (unsigned int i = 0; i < x.size(); i++) {x[i] -= average;}


	return x;
}

// Normalize vector x, Formula (4) -- openMP A ML
vector<float> normalize(vector<float> x, float *length) {

	float sum = 0;

	bool useOpenMpFlag = (x.size() > param_minOpenmp);

	#pragma omp parallel for reduction(+: sum) if (useOpenMpFlag)
	for (unsigned int i = 0; i < x.size(); i++) {sum += x[i]*x[i];}

	*length = sqrt(sum);
	if (*length == 0) {*length = 0.000000001;
	//cerr << "normalize" << endl;
	}// ATTENTION not 0!

	#pragma omp parallel for if (useOpenMpFlag)
	for (unsigned int i = 0; i < x.size(); i++) {x[i] /= *length;}

	return x;
}

// Qx, Formula (5) -- openMP A ML
vector<float> getY(float max_degree, vector<float> x_hat, vector<float> x_new, vector<unsigned int>& nodes, vector<unsigned int>& diag){

	bool useOpenMpFlag = (nodes.size() > param_minOpenmp);

	#pragma omp parallel for if (useOpenMpFlag)
	for (unsigned int i = 0; i < x_hat.size(); i++) {

		x_hat[i] *= (2*max_degree - diag[i]);
		x_hat[i] += x_new[i];

	}

	return x_hat;
}

unsigned int sumOutDegs(const vector<unsigned int>& nodes) {

	unsigned int sum = 0;
	for (unsigned int a = 0; a < nodes.size(); a++) {
		unsigned int from = nodes[a];
		sum += graph[from].edges.size();
	}

	return sum/2;
}


int main(int argc, char *argv[]) {

	// check for an argument
	if (argc<5){
		PRINTHELP:
		cout << "Usage: " << argv[0] << " 01_USEWEIGHTS 012_modus_0_power_1_ssyevr_2_mcl_3_dsyevr EPSILON CPUTHREADS BLASTGRAPH " << endl;
		cout << "OUTPUT: numNodes,numEdges,connectivity(EV)" << endl;
		return -1;
	}

	try{

		param_useWeights = (bool)string2float(argv[1]);
		param_useLapack = (int)string2float(argv[2]);
		epsilon = string2float(argv[3]);
		
		omp_set_dynamic(0);     // Explicitly disable dynamic teams
		omp_set_num_threads(string2float(argv[4])); 

		// read in a text file that contains a real matrix stored in column major format
		// but read it into row major format
		if(param_useLapack!=2)
			parse_file(argv[5]);

		std::vector<unsigned int > nodes;
		for(unsigned int i = 0 ; i < graph.size() ; i++){
			nodes.push_back(i);
		}

		int n=0;
		if(param_useLapack!=2)
			n=nodes.size();

		float maxWeight=-1;
		map<unsigned int,unsigned int> mapping;
		for (unsigned int i = 0; i < (unsigned int)n; i++) {mapping[nodes[i]] = i;}

		float connectivity = -1;
		vector<float> x_hat(n);

		bool useOpenMpFlag = ((unsigned int)n > param_minOpenmp);

		if( param_useLapack==1 && n>32768-1-1 ){
			goto done_flag;
		}


		if( param_useLapack==2 ){

			// maximal number of nodes 32768 (2^15) -> laplace matrix 2^15*2^15=2^30 entries 
			// used ram in MB of lapack = (unsigned int)n*(unsigned int)n*sizeof(float))/1e+6 

			// float * laplacian = (float*)calloc( (unsigned int)n*(unsigned int)n,sizeof(float) );

			// bool fill_laplacian_return=1;
			// // fill laplacian
			// for(unsigned int i = 0 ; i < (unsigned int)n ; i++){

			// 	unsigned int from = nodes[i]; 
			// 	unsigned int sum = 0;

			// 	for(unsigned int j = 0 ; j < graph[from].edges.size() ; j++){

			// 		unsigned int to = graph[from].edges[j].edge;
			// 		unsigned int vector_idx = mapping[from] + mapping[to]*n; //transform the 2-d coordinate (i,j) of the nxn matrix to 1-d vector coordinate i+j*n of the 2n vector 

			// 		if( vector_idx >= (unsigned int)n*(unsigned int)n){
			// 			fill_laplacian_return = -1;
			// 			break;
			// 		}

			// 		if( param_useWeights 
			// 			// && (unsigned int)n <= maxUseWeightsNumNodes 
			// 			){
			// 			float w = graph[from].edges[j].weight;
			// 			sum+=w;
			// 			laplacian[vector_idx]=-w;
			// 			if(maxWeight<w)maxWeight=w;
			// 		}else{
			// 			sum++;
			// 			laplacian[vector_idx]=-1.0;
			// 		}
			// 	}
			// 	laplacian[mapping[from]+mapping[from]*n]=sum;
			// }
			// if(!fill_laplacian_return){
			// 	cerr <<"out of range" << endl;
			// 	return -1;
			// }

			// // local variables:
			// int il, iu, m, lda = n, ldz = n, info, lwork;
			// float vl, vu;
			// float wkopt;
			// float* work;
			// char Vchar='V', Ichar='I', Uchar='U';
			// int iwork[(unsigned int)5*(unsigned int)n], ifail[n]; // iwork dimension should be at least 5*n 
			// float eigenvalues[n];
			// float * eigenvectors = (float*)malloc( (unsigned int)ldz*(unsigned int)n*sizeof(float) );
			// il = 2;
			// iu = 2;
			// // Determine optimal workspace 
			// lwork = -1;
			// dsyevx_( &Vchar, &Ichar, &Uchar, &n,laplacian, &lda, &vl, &vu, &il, &iu, &epsilon, &m, eigenvalues, eigenvectors, &ldz, &wkopt, &lwork, iwork, ifail, &info );
			// lwork = (int)wkopt;
			// work = (float*)malloc( lwork*sizeof(float) );
			// /* Solve eigenproblem */
			
			// dsyevx_( &Vchar, &Ichar, &Uchar, &n, laplacian, &lda, &vl, &vu, &il, &iu,&epsilon, &m,  eigenvalues, eigenvectors, &ldz, work, &lwork, iwork, ifail, &info );

			// // Check for convergence
			// if( info > 0 ) {
			// 	printf( " [WARNING] The algorithm dsyevx failed to compute eigenvalues. Continue now with the slow standard approach (power iteration).\n" );
			// 	return -1;
			// }

			// // deallocate
			// delete [] laplacian;
			// delete [] work;

			// // calculate normal algebraic connectivity and fill x_hat
			// if(param_useWeights){
			// 	connectivity = eigenvalues[0]/(maxWeight*(float)n);
			// }else{
			// 	connectivity = eigenvalues[0]/((float)n);
			// }
			// for(unsigned int i = 0 ; i < (unsigned int)n ; i++)
			// 	x_hat[i]=eigenvectors[i];

			// // deallocate
			// delete [] eigenvectors;	

		}else if( param_useLapack==1 && n<32768-1 ){

			// cerr << "doing ssyevr"<< endl;
			// maximal number of nodes 32768 (2^15) -> laplace matrix 2^15*2^15=2^30 entries 
			// used ram in MB of lapack = (unsigned int)n*(unsigned int)n*sizeof(float))/1e+6 

			float * laplacian = (float*)calloc( (unsigned int)n*(unsigned int)n,sizeof(float) );

			bool fill_laplacian_return=1;
			// fill laplacian
			for(unsigned int i = 0 ; i < (unsigned int)n ; i++){

				unsigned int from = nodes[i]; 
				unsigned int sum = 0;

				for(unsigned int j = 0 ; j < graph[from].edges.size() ; j++){

					unsigned int to = graph[from].edges[j].edge;
					unsigned int vector_idx = mapping[from] + mapping[to]*n; //transform the 2-d coordinate (i,j) of the nxn matrix to 1-d vector coordinate i+j*n of the 2n vector 

					if( vector_idx >= (unsigned int)n*(unsigned int)n){
						fill_laplacian_return = -1;
						break;
					}

					if( param_useWeights 
						// && (unsigned int)n <= maxUseWeightsNumNodes 
						){
						float w = graph[from].edges[j].weight;
						sum+=w;
						laplacian[vector_idx]=-w;
						if(maxWeight<w)maxWeight=w;
					}else{
						sum++;
						laplacian[vector_idx]=-1.0;
					}
				}
				laplacian[mapping[from]+mapping[from]*n]=sum;
			}
			if(!fill_laplacian_return){
				cerr <<"out of range" << endl;
				return -1;
			}

			// local variables:
			int il, iu, m, lda = n, ldz = n, info, lwork, liwork, iwkopt;
			float vl, vu;
			float wkopt;
			float* work;
	        int* iwork;
	        int isuppz[n];
			char Vchar='V', Ichar='I', Uchar='U';
			float eigenvalues[n];
			float * eigenvectors = (float*)malloc( (unsigned int)ldz*(unsigned int)n*sizeof(float) );
			il = 2;
			iu = 2;
			// Determine optimal workspace 
			lwork = -1;
	        liwork = -1;
			ssyevr_( &Vchar, &Ichar, &Uchar, &n,laplacian, &lda, &vl, &vu, &il, &iu, &epsilon, &m, eigenvalues, eigenvectors, &ldz, isuppz, &wkopt, &lwork, &iwkopt, &liwork, &info );

	        lwork = (int)wkopt;
	        work = (float*)malloc( lwork*sizeof(float) );
	        liwork = iwkopt;
	        iwork = (int*)malloc( liwork*sizeof(int) );

			/* Solve eigenproblem */
			
			ssyevr_( &Vchar, &Ichar,&Uchar, &n, laplacian, &lda, &vl, &vu, &il, &iu,&epsilon, &m, eigenvalues, eigenvectors, &ldz, isuppz, work, &lwork, iwork, &liwork , &info );

			// Check for convergence
			if( info > 0 ) {
				printf( " [WARNING] The algorithm dsyevr failed to compute eigenvalues. Continue now with the slow standard approach (power iteration).\n" );
				return -1;
			}

			// deallocate
			delete [] laplacian;
			delete [] work;
			delete [] iwork;

			// calculate normal algebraic connectivity and fill x_hat
			if(param_useWeights){
				connectivity = eigenvalues[0]/(maxWeight*(float)n);
			}else{
				connectivity = eigenvalues[0]/((float)n);
			}
			for(unsigned int i = 0 ; i < (unsigned int)n ; i++)
				x_hat[i]=eigenvectors[i];

			// deallocate
			delete [] eigenvectors;	

		}else if( param_useLapack==3 && n<32768-1 ){

			// cerr << "doing dsyevr"<< endl;
			// maximal number of nodes 32768 (2^15) -> laplace matrix 2^15*2^15=2^30 entries 
			// used ram in MB of lapack = (unsigned int)n*(unsigned int)n*sizeof(double))/1e+6 

			double * laplacian = (double*)calloc( (unsigned int)n*(unsigned int)n,sizeof(double) );

			bool fill_laplacian_return=1;
			// fill laplacian
			for(unsigned int i = 0 ; i < (unsigned int)n ; i++){

				unsigned int from = nodes[i]; 
				unsigned int sum = 0;

				for(unsigned int j = 0 ; j < graph[from].edges.size() ; j++){

					unsigned int to = graph[from].edges[j].edge;
					unsigned int vector_idx = mapping[from] + mapping[to]*n; //transform the 2-d coordinate (i,j) of the nxn matrix to 1-d vector coordinate i+j*n of the 2n vector 

					if( vector_idx >= (unsigned int)n*(unsigned int)n){
						fill_laplacian_return = -1;
						break;
					}

					if( param_useWeights 
						// && (unsigned int)n <= maxUseWeightsNumNodes 
						){
						double w = graph[from].edges[j].weight;
						sum+=w;
						laplacian[vector_idx]=-w;
						if(maxWeight<w)maxWeight=w;
					}else{
						sum++;
						laplacian[vector_idx]=-1.0;
					}
				}
				laplacian[mapping[from]+mapping[from]*n]=sum;
			}
			if(!fill_laplacian_return){
				cerr <<"out of range" << endl;
				return -1;
			}

			// local variables:
			int il, iu, m, lda = n, ldz = n, info, lwork, liwork, iwkopt;
			double vl, vu;
			double wkopt;
			double* work;
	        int* iwork;
	        int isuppz[n];
			char Vchar='V', Ichar='I', Uchar='U';
			double eigenvalues[n];
			double * eigenvectors = (double*)malloc( (unsigned int)ldz*(unsigned int)n*sizeof(double) );
			il = 2;
			iu = 2;
			// Determine optimal workspace 
			lwork = -1;
	        liwork = -1;
	        double epsilon_d=epsilon;
			dsyevr_( &Vchar, &Ichar, &Uchar, &n,laplacian, &lda, &vl, &vu, &il, &iu, &epsilon_d, &m, eigenvalues, eigenvectors, &ldz, isuppz, &wkopt, &lwork, &iwkopt, &liwork, &info );

	        lwork = (int)wkopt;
	        work = (double*)malloc( lwork*sizeof(double) );
	        liwork = iwkopt;
	        iwork = (int*)malloc( liwork*sizeof(int) );

			/* Solve eigenproblem */
			
			dsyevr_( &Vchar, &Ichar,&Uchar, &n, laplacian, &lda, &vl, &vu, &il, &iu,&epsilon_d, &m, eigenvalues, eigenvectors, &ldz, isuppz, work, &lwork, iwork, &liwork , &info );

			// Check for convergence
			if( info > 0 ) {
				printf( " [WARNING] The algorithm dsyevr failed to compute eigenvalues. Continue now with the slow standard approach (power iteration).\n" );
				return -1;
			}

			// deallocate
			delete [] laplacian;
			delete [] work;
			delete [] iwork;

			// calculate normal algebraic connectivity and fill x_hat
			if(param_useWeights){
				connectivity = eigenvalues[0]/(maxWeight*(double)n);
			}else{
				connectivity = eigenvalues[0]/((double)n);
			}
			for(unsigned int i = 0 ; i < (unsigned int)n ; i++)
				x_hat[i]=eigenvectors[i];

			// deallocate
			delete [] eigenvectors;	

		}else{

			// cerr << "doing power"<< endl;
			bool useWeights = param_useWeights;// (param_useWeights && nodes.size() <= maxUseWeightsNumNodes); //param_useWeights = user input whether weights should be used or not. useWeights = the true value, that is true if param_useWeights is true and the maximum number of nodes are not exeeded for the weighted algorithm (maxUseWeightsNumNodes)

			// if(param_useWeights && !useWeights){
			// 	cerr << " [WARNING] The maximum number of nodes for the weighted algorithm is exeeded. Continue now with the faster unweighted algorithm." << endl;
			// }
			//diagonal matrix diag : d(u,u)=number of adjacency nodes=deg(u)
			vector<unsigned int> diag(n);

			if(useWeights){
				for (unsigned int i = 0; i < (unsigned int)n; i++) {
					diag[i]=0;
					for (unsigned int j = 0; j < graph[nodes[i]].edges.size(); j++) {
						diag[i] += graph[nodes[i]].edges[j].weight;
						if(useWeights && maxWeight<graph[nodes[i]].edges[j].weight)maxWeight=graph[nodes[i]].edges[j].weight;
					}
				}
			}else{
				for (unsigned int i = 0; i < (unsigned int)n; i++) {
					diag[i]=graph[nodes[i]].edges.size();
					if(useWeights)
						for (unsigned int j = 0; j < graph[nodes[i]].edges.size(); j++) {
							if(maxWeight<graph[nodes[i]].edges[j].weight)maxWeight=graph[nodes[i]].edges[j].weight;
						}
				}
			}

			// Get max degree / sum of weights of nodes
			unsigned int max_d = max_of_diag(nodes,diag);	

			// Init randomized variables. 
			vector<float> norm;

			vector<float> x = generate_random_vector(n);
			

			// Orthogonalize + normalize vector + get initial lenght
			float current_length = 0;
			float last_length;

			x_hat = makeOrthogonal(x);
			norm = normalize(x_hat, &last_length);


			// Repeat until difference < epsilon
			unsigned int iter = 0;	// catch huge clustering issues by keeping track here

			while(++iter < max_iter) { 

				// if (debug_level > 2 && iter%100 == 0) cerr << getTime() << " [DEBUG L2] Step " << iter << " / " << max_iter << endl;
				last_length = current_length;

				// Get a new x
				x = get_new_x(norm, nodes, mapping, useWeights);

				// Get y
				vector<float> y = getY(max_d,norm,x,nodes,diag);

				// Orthogonalize
				x_hat = makeOrthogonal(y);

				// Get lenght (lambda) & normalize vector
				norm = normalize(x_hat, &current_length);

				if ( abs(current_length-last_length) < epsilon && iter >= min_iter ) break;	// prevent convergence by chance, converge to epsilon
			}


			// if (debug_level > 0) cerr << getTime() << " [DEBUG]   " << iter << " / " << max_iter << " iterations required (error is " << abs(current_length-last_length) << ")" << endl;

			#ifdef DEBUG
				total_number_of_iterations_convergence+=iter;
			#endif

			if(useWeights){
				connectivity = (-current_length+2*max_d)/(maxWeight*(float)n);
			}else{
				connectivity = (-current_length+2*max_d)/(n);
			}
			x_hat = normalize(x_hat, &current_length);
			
				
			// 5.17 catch hardly-converging groups
			// if (iter >= max_iter) {
				// if (debug_level > 0) cerr << getTime() << " [DEBUG]   Connectivity score of connected component with " << n << " elements did not converge perfectly in time." << endl;
			// }
		}

		done_flag:

		cout << nodes.size() << "," <<edges << "," << connectivity << endl;

		if(printEV){
			cout << "(";
			for(unsigned int i = 0 ; i < (unsigned int)n ; i++){
				cout << x_hat[i]; 
				if(i!=n-1)
					cout << ","; 
			}
			cout << ")"<< endl;
		}

	}catch(string& error) {
		cerr << "[ERROR] catched " << error << endl;

		goto PRINTHELP;
		return EXIT_FAILURE;
	}

	return 0;
}
