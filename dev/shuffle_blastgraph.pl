#!/usr/bin/perl
use List::Util qw/shuffle/;

open(FILEIN,"<",$ARGV[0]);
@arr=();
while(<FILEIN>){
	if(length($_)<1){continue;}
	if(m/^#/){
		if(scalar(@arr)>0){
			@arr = shuffle @arr;
			foreach $k (@arr){
				print $k;
			}	
		}
		print $_;
		@arr=();
	}else{
		push(@arr,$_);
	}
}
@arr = shuffle @arr;
foreach $k (@arr){
	print $k;
}
close(FILEIN);