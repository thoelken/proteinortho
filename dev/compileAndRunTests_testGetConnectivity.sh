#!/bin/bash

for filename in *.sav; do

	echo $filename >>out11.sav;
	echo $filename >>out01.sav;
	echo $filename >>out10.sav;
	echo $filename >>out00.sav;

    for ((i=0; i<=10; i++)); do
    	
    	ret=$(/usr/bin/time -f "%e	%M" ./test_getConnectivity 1 1 0 $filename 2>&1)
		echo $ret >>out11.sav;
    	
    done

    for ((i=0; i<=10; i++)); do
    	
    	ret=$(/usr/bin/time -f "%e,%M" ./test_getConnectivity 0 1 0 $filename 2>&1)
		echo $ret >>out01.sav;
    	
    done

    for ((i=0; i<=10; i++)); do
    	
    	ret=$(/usr/bin/time -f "%e,%M" ./test_getConnectivity 1 0 0 $filename 2>&1)
		echo $ret >>out10.sav;
    	
    done

    for ((i=0; i<=10; i++)); do
    	
    	ret=$(/usr/bin/time -f "%e,%M" ./test_getConnectivity 0 0 0 $filename 2>&1)
		echo $ret >>out00.sav;
    	
    done

	# echo $runtime >> $outpath/log_$name.txt

done
