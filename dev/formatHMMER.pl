#!/usr/bin/perl

use warnings;
use strict;

unless ($ARGV[0]) {
	print STDERR "Usage: formatHMMER.pl FILE_A\n\nExpecting hmmer tabout format file FILE_A. COUTing the blast tab 6 format.\n";
	exit;
}

my %a;
open(FILE,"<$ARGV[0]") || die("Error, could not open file $ARGV[0]: $!");
while(<FILE>) {
	chomp;
	if ($_ =~ /^#/) {next;}
	my @row = split(/\s+/);
	my @full = map {$_ ? $_ : ()} @row;
	print STDOUT $row[0]."\t".$row[2]."\t0\t0\t0\t0\t0\t0\t0\t0\t".$row[4]."\t".$row[5]."\n";
}
close(FILE);
