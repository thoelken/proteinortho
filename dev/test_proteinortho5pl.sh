#!/bin/bash
if [ "$1" == "blastp" ]; then
	mkdir blastp+

	start=`date +%s%N`
	perl proteinortho5.pl -project=time_blastp+ -step=1 -p=blastp+ -tmp=blastp+ -keep fasta/*.fasta
	perl proteinortho5.pl -project=time_blastp+ -step=2 -p=blastp+ -tmp=blastp+ -keep fasta/*.fasta
	end=`date +%s%N`
	runtime=$(( end-start ))
	echo "blastp+	"$runtime >> time_log.txt

elif [ "$1" == "diamond" ]; then

	mkdir diamond

	start=`date +%s%N`
	perl proteinortho5.pl -project=time_diamond -step=1 -p=diamond -tmp=diamond -keep fasta/*.fasta
	perl proteinortho5.pl -project=time_diamond -step=2 -p=diamond -tmp=diamond -keep fasta/*.fasta
	end=`date +%s%N`
	runtime=$(( end-start ))
	echo "diamond	"$runtime >> time_log.txt

elif [ "$1" == "diamondmoresensitive" ]; then

	mkdir diamondmoresensitive

	start=`date +%s%N`
	perl proteinortho5.pl -project=time_diamondmoresensitive -step=1 -tmp=diamondmoresensitive -p=diamondmoresensitive -keep fasta/*.fasta
	perl proteinortho5.pl -project=time_diamondmoresensitive -step=2 -tmp=diamondmoresensitive -p=diamondmoresensitive -keep fasta/*.fasta
	end=`date +%s%N`
	runtime=$(( end-start ))
	echo "diamondmoresensitive	"$runtime >> time_log.txt

elif [ "$1" == "phmmer" ]; then

	mkdir phmmer

	start=`date +%s%N`
	perl proteinortho5.pl -project=time_phmmer -step=1 -p=phmmer -tmp=phmmer -keep fasta/*.fasta
	perl proteinortho5.pl -project=time_phmmer -step=2 -p=phmmer -tmp=phmmer -keep fasta/*.fasta
	end=`date +%s%N`
	runtime=$(( end-start ))
	echo "phmmer	"$runtime >> time_log.txt

elif [ "$1" == "jackhmmer" ]; then

	mkdir jackhmmer

	start=`date +%s%N`
	perl proteinortho5.pl -project=time_jackhmmer -step=1 -p=jackhmmer -tmp=jackhmmer -keep fasta/*.fasta
	perl proteinortho5.pl -project=time_jackhmmer -step=2 -p=jackhmmer -tmp=jackhmmer -keep fasta/*.fasta
	end=`date +%s%N`
	runtime=$(( end-start ))
	echo "jackhmmer	"$runtime >> time_log.txt

elif [ "$1" == "lastp" ]; then

	mkdir lastp

	start=`date +%s%N`
	perl proteinortho5.pl -project=time_lastp -step=1 -p=lastp -tmp=lastp -keep fasta/*.fasta
	perl proteinortho5.pl -project=time_lastp -step=2 -p=lastp -tmp=lastp -keep fasta/*.fasta
	end=`date +%s%N`
	runtime=$(( end-start ))
	echo "lastp	"$runtime >> time_log.txt

elif [ "$1" == "topaz" ]; then

	mkdir topaz

	start=`date +%s%N`
	perl proteinortho5.pl -project=time_topaz -step=1 -p=topaz -tmp=topaz -keep fasta/*.fasta
	perl proteinortho5.pl -project=time_topaz -step=2 -p=topaz -tmp=topaz -keep fasta/*.fasta
	end=`date +%s%N`
	runtime=$(( end-start ))
	echo "topaz	"$runtime >> time_log.txt

elif [ "$1" == "rapsearch" ]; then

	mkdir rapsearch

	start=`date +%s%N`
	perl proteinortho5.pl -project=time_rapsearch -step=1 -p=rapsearch -tmp=rapsearch -keep fasta/*.fasta
	perl proteinortho5.pl -project=time_rapsearch -step=2 -p=rapsearch -tmp=rapsearch -keep fasta/*.fasta
	end=`date +%s%N`
	runtime=$(( end-start ))
	echo "rapsearch	"$runtime >> time_log.txt

elif [ "$1" == "usearch" ]; then

	mkdir usearch

	start=`date +%s%N`
	perl proteinortho5.pl -project=time_usearch -step=1 -p=usearch -tmp=usearch -keep fasta/*.fasta
	perl proteinortho5.pl -project=time_usearch -step=2 -p=usearch -tmp=usearch -keep fasta/*.fasta
	end=`date +%s%N`
	runtime=$(( end-start ))
	echo "usearch	"$runtime >> time_log.txt

elif [ "$1" == "ublast" ]; then

	mkdir ublast

	start=`date +%s%N`
	perl proteinortho5.pl -project=time_ublast -step=1 -p=ublast -tmp=ublast -keep fasta/*.fasta
	perl proteinortho5.pl -project=time_ublast -step=2 -p=ublast -tmp=ublast -keep fasta/*.fasta
	end=`date +%s%N`
	runtime=$(( end-start ))
	echo "ublast	"$runtime >> time_log.txt

fi

#sort time_log.txt -k2,2g >asd.txt
#mv asd.txt time_log.txt
