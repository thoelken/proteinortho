// compile g++ generateARandomBlastGraph.cpp -o generateARandomBlastGraph
// run : ./generateARandomBlastGraph 5 0.8 -> n=5 density=0.8
// run : ./generateARandomBlastGraph 5 0.8 10 2 0.9 2 -> n=5,8,10 density=0.8,0.9
// the 2 corresponds to the number of steps 
//pk
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>     /* srand, rand */
#include <map>

using namespace std;

double string2double(string str) {
	istringstream buffer(str);
	double value;
	buffer >> value;
	return value;
}
template<typename T> string num2string(T d) {
	std::ostringstream strs;
	strs << d;
	return strs.str();
}
pair<int,int> orderPair(int a, int b){if(a<b)return make_pair(a,b); else return make_pair(b,a);}

int main(int argc, char *argv[]) {

	if(argc != 7 && argc != 3){
		PRINTHELP:
		cout << "USAGE:"<< "generateARandomBlastGraph 5 0.8" << endl << " -> generate a random graph with n=5 nodes and a 80% density (percentage that a edge exists). Edge weights (bitscores) are sampled between 0 and 2000, evalue is 1/bitscore." << endl << "./generateARandomBlastGraph 5 0.8 10 3 0.9 2" << "-> generate graphs with 5,8,10 nodes and densities of 0.8 and 0.9 (3 steps in the number of nodes and 2 steps in density)." << "n_start d_start n_stop n_numberOfInc d_stop d_numberOfInc" << endl;
		return -1;
	}

	try{

		int n_start = string2double(argv[1]);
		double d_start = string2double(argv[2]);

		int n_end=n_start,n_inc=1;
		double d_end=d_start,d_inc=1;

		if(argc==7){
			n_end = string2double(argv[3]);
			if(string2double(argv[4])>=2.0)
				n_inc= ((double)n_end-(double)n_start)/(string2double(argv[4])-1.0);
			d_end= string2double(argv[5]);
			if(string2double(argv[6])>=2.0)
				d_inc= (d_end-d_start)/(string2double(argv[6])-1.0);
		}

		for(int n = n_start ; n < n_end+n_inc ; n+=n_inc){
			if(n>n_end)n=n_end;
			for(double d = d_start ; d < d_end+d_inc ; d+=d_inc){

				cout << ("Gn"+num2string<int>(n)+"_d"+num2string<double>(d)+".sav") << endl;

			  	ofstream myfile( ("Gn"+num2string<int>(n)+"_d"+num2string<double>(d)+".sav").c_str() );
				myfile << "# file_a	file_b\n";
				myfile << "# a	b	evalue_ab	bitscore_ab	evalue_ba	bitscore_ba\n";

				map<pair<int,int> , bool > E;
				if(d<0.6){
					while( (double)E.size() < d * ((double)n*(double)(n-1))/2.0 ){
						int va=0, vb=0;

						va=rand() % n;
						vb=rand() % n;

						while( va==vb || E.count(orderPair(va,vb))){
							vb=rand() % n;
						}

						myfile << va << "	"<<vb<<"	"<<1.0/((double)(rand()%2000))<<"	"<<(rand()%2000)<<"	"<<1.0/((double)(rand()%2000))<<"	"<<(rand()%2000)<<"\n";

						E[orderPair(va,vb)]=1;

					}
				}else{
					for(unsigned int i = 0 ; i < n ; i++){
						for(unsigned int j = i+1 ; j < n ; j++){
							E[orderPair(i,j)]=1;
						}
					}
					while( (double)E.size() > d * ((double)n*(double)(n-1))/2.0 ){


						int va=0, vb=0;

						va=rand() % n;
						vb=rand() % n;

						while( va==vb || !E.count(orderPair(va,vb))){
							vb=rand() % n;
						}
						map<pair<int,int>,bool>::iterator it = E.find(orderPair(va,vb));
						E.erase (it);
					}
					for(map<pair<int,int>,bool>::iterator it = E.begin(); it != E.end() ; ++it){
						myfile << (*it).first.first << "	"<<(*it).first.second<<"	"<<1.0/((double)(rand()%2000))<<"	"<<(rand()%2000)<<"	"<<1.0/((double)(rand()%2000))<<"	"<<(rand()%2000)<<"\n";
					}
				}
				
				myfile.close();

			}

		}

	}catch(string& error) {
		cerr << "[ERROR] catched " << error << endl;
		goto PRINTHELP;
		return EXIT_FAILURE;
	}
	return 0;
}
