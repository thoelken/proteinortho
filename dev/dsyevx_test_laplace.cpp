//compile: g++ dsyevx_test_laplace.cpp -o dsyevx_test_laplace -llapack
// g++ -O3 -mtune=native -march=native dsyevx_test_laplace.cpp -o dsyevx_test_laplace -llapack
//run: ./dsyevx_test_laplace matrix.txt
//pk
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>

using namespace std;

struct wedge {unsigned int edge; unsigned short weight;};
struct protein {vector<wedge> edges; unsigned int species_id; string full_name;};
// Globals
unsigned int species_counter = 0; // Species
unsigned int protein_counter = 0; // Proteins
vector<string> species;     // Number -> Name
vector<protein> graph;      // Graph containing all protein data
double last_stat = 0;     // For progress stats
unsigned int edges = 0;     // number of edges

// TMP Globals
map<string,int> species2id;   // Name -> Number
map<string,int> protein2id;   // Name -> Number



extern "C" {

//dsyevx 
//-      double
// --    symmetric 
//   --- eigenvalue expert (-> first k eigenvalues...)
extern int dsyevx_(char*, //jobz 'V':  Compute eigenvalues and eigenvectors.
                  char*, //RANGE 'I': the IL-th through IU-th eigenvalues will be found.
                  char*, //?? UPLO 'U'/'L'
                  int*, //N The order of the matrix A.  N >= 0.
                  double*, // AP
                  int*, // LDA 
                  double*, //if range V
                  double*, //if range V
                  int*, //IL smallest eigenvalue to be returned.1 <= IL <= IU <= N
                  int*, //IU largest eigenvalue to be returned.1 <= IL <= IU <= N
                  double*, // ABSTOL The absolute error tolerance for the eigenvalues.
                  int*, //M The total number of eigenvalues found. 'I', M = IU-IL+1
                  double*, //W DOUBLE PRECISION array, dimension (N)
                  double*, //Z DOUBLE PRECISION array, dimension (LDZ, max(1,M))
                  int*, //LDZ
                  double*, //WORK
                  int*, //IWORK
                  int*, //IFAIL If INFO > 0, then IFAIL contains the indices of the eigenvectors that failed to converge.If JOBZ = 'N', then IFAIL is not referenced.
                  int*, //IFAIL If INFO > 0, then IFAIL contains the indices of the eigenvectors that failed to converge.If JOBZ = 'N', then IFAIL is not referenced.
                  int*); //INFO is
}

///////////////////////////////////////////////////////////
// Misc functions
///////////////////////////////////////////////////////////
// Convert string to double
double string2double(string str) {
  istringstream buffer(str);
  double value;
  buffer >> value;
  return value;
}


// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {
  #ifdef timeAnalysis
    auto t_tmp = std::chrono::steady_clock::now( );
  #endif

  // Skip delimiters at beginning.
  string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  string::size_type pos = str.find_first_of(delimiters, lastPos);

  while (string::npos != pos || string::npos != lastPos) {
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }

  #ifdef timeAnalysis
    if(!t_master.count("tokenize")) t_master["tokenize"]=0;
    t_master["tokenize"] += (double)std::chrono::duration_cast<std::chrono::nanoseconds>( std::chrono::steady_clock::now( ) - t_tmp ).count()/1e+9;
  #endif
}

///////////////////////////////////////////////////////////
// File parser
///////////////////////////////////////////////////////////
void parse_file(string file) {

  string line;
  ifstream graph_file(file.c_str());
  if (graph_file.is_open()) {
    // For each line
    string file_a = ""; unsigned int file_a_id = 0;
    string file_b = ""; unsigned int file_b_id = 0;
    while (!graph_file.eof()) {
      getline(graph_file, line);
      vector<string> fields;
      tokenize(line, fields, "\t");
      // Header line
      if (fields.size() == 2 && fields[0].substr(0, 1) == "#") {
        file_a = fields[0].substr(2, fields[0].size()-2);
        file_b = fields[1];

        if (file_a == "file_a" && file_b == "file_b") continue; // Init Header

        // Map species a
        if (species2id.find(file_a) == species2id.end())  {
            species.push_back(file_a);
            species2id[file_a] = species_counter++;
        }
        // Map species b
        if (species2id.find(file_b) == species2id.end())  {
            species.push_back(file_b);
            species2id[file_b] = species_counter++;
        }

        file_a_id = species2id[file_a];
        file_b_id = species2id[file_b];
      }
      // Data line
      else if ((fields.size() == 6 || fields.size() == 8) && fields[0].substr(0, 1) != "#") {
        // a b e1 b1 e2 b2 score

        // 5.16 deal with duplicated IDs by adding file ID to protein ID
        string ida = fields[0];
        string idb = fields[1];
        fields[0] += " "; fields[0] += file_a_id;
        fields[1] += " "; fields[1] += file_b_id;

        // 5.16 do not point to yourself
        if (!fields[0].compare(fields[1])) {continue;}

        // A new protein
        if (protein2id.find(fields[0]) == protein2id.end()) {
          protein a;
          a.full_name = ida;
          a.species_id  = file_a_id;
          protein2id[fields[0]] = protein_counter++;
          graph.push_back(a);
        }
        if (protein2id.find(fields[1]) == protein2id.end()) {
          protein b;
          b.full_name = idb;
          b.species_id  = file_b_id;
          protein2id[fields[1]] = protein_counter++;
          graph.push_back(b);
        }

        // Bitscores 
        // check range (in double)
        double bit_a = string2double(fields[3]);
        double bit_b = string2double(fields[5]);
        if(bit_a>65535)
          throw string(" ERROR unsigned short overflow " +fields[3] + " (bitscore of "+ida+")");
        if(bit_b>65535)
          throw string(" ERROR unsigned short overflow " +fields[5] + " (bitscore of "+idb+")");

        // assign
        unsigned short bitscore_a = bit_a;
        unsigned short bitscore_b = bit_b;
        unsigned short bitscore_avg = (bit_a+bit_b)/2;
    
        // Add link to graph (reciprocal)         
        unsigned int a_id = protein2id[fields[0]];
        unsigned int b_id = protein2id[fields[1]];

        // 5.17, add weight
        wedge w;
        w.edge=b_id;
        w.weight=bitscore_avg;
        graph[a_id].edges.push_back(w);
        w.edge=a_id;
        w.weight=bitscore_avg;
        graph[b_id].edges.push_back(w);
        edges++;
      }
    }
  }
  else {
    throw string("Could not open file " + file);
  }

}

/* Auxiliary routine: printing a matrix */
void print_matrix( int m, int n, double* a, int lda ) {
        int i, j;
        for( i = 0; i < m; i++ ) {
                for( j = 0; j < n; j++ ) printf( " %6.2f", a[i+j*lda] );
                printf( "\n" );
        }
}

int main(int argc, char** argv){

  // check for an argument
  if (argc<2){
    cout << "Usage: " << argv[0] << " " << " filename" << endl;
    return -1;
  }

  // read in a text file that contains a real matrix stored in column major format
  // but read it into row major format
  parse_file(argv[1]);

  vector<unsigned int> nodes;
  for(unsigned int i = 0 ; i < graph.size(); i++){
    nodes.push_back(i);
    //if(i>1000-1-1)break;
    if(i>32768-1-1)break;
  }
  cout << nodes.size() << " from " << graph.size() << endl;


  {
    cerr << "done loading graph, now building L" << endl;
    int n=nodes.size();
    // if(n>1047337){
    //   cerr << "overflow" << endl;
    //   return -1;
    // }

    // cerr << n << " " << (unsigned int)n*(unsigned int)n << " " <<n*n*sizeof(double)<< endl;

  map<unsigned int,unsigned int> mapping;
  for (unsigned int i = 0; i < nodes.size(); i++) {mapping[nodes[i]] = i;}

    //double * laplacian = new double[n2];
    double * laplacian = (double*)calloc( (unsigned int)n*(unsigned int)n,sizeof(double) );
    //std::vector<double> laplacian((unsigned int)n*(unsigned int)n);

    for(unsigned int i = 0 ; i < n ; i++){
      unsigned int from = nodes[i];
      unsigned int sum = 0;
      for(unsigned int j = 0 ; j < graph[from].edges.size() ; j++){

        unsigned int to = graph[from].edges[j].edge;

        if(mapping[from]+mapping[to]*n >= (unsigned int)n*(unsigned int)n)continue;

        //cout << from << " " << to << " " << from+to*n << endl;

        unsigned int w = graph[from].edges[j].weight;

        sum+=w;
        //sum++;
        laplacian[mapping[from]+mapping[to]*n]=-(double)w;
        //laplacian[mapping[from]+mapping[to]*n]=-1;
      }
      laplacian[mapping[from]+mapping[from]*n]=sum;

      cerr << from << " " << sum << endl;

    }

print_matrix(n,n,laplacian,n);

    cerr << "done with L, now converging" << endl;

    int il, iu, m, lda = n, ldz = n, info, lwork;
    double abstol, vl, vu;
    double wkopt;
    double* work;
    char Nchar='N';
    char Vchar='V';
    char Ichar='I';
    char Uchar='U';
    char Lchar='L';
    char Achar='A';
    /* Local arrays */
    /* iwork dimension should be at least 5*n */
    int iwork[(unsigned int)5*(unsigned int)n], ifail[n];
    double eigenvalues[n];//, eigenvectors[(unsigned int)ldz*(unsigned int)n];
    double * eigenvectors = (double*)malloc( (unsigned int)1*(unsigned int)n*sizeof(double) );
    /* Executable statements */
    /* Negative abstol means using the default value */
    abstol = 1e-8;
    /* Set il, iu to compute NSELECT smallest eigenvalues */
    il = 2;
    iu = 2;
    /* Query and allocate the optimal workspace */
    lwork = -1;
    dsyevx_( &Vchar, &Ichar, &Uchar, &n,laplacian, &lda, &vl, &vu, &il, &iu, &abstol, &m, eigenvalues, eigenvectors, &ldz, &wkopt, &lwork, iwork, ifail, &info );
    lwork = (int)wkopt;
    work = (double*)malloc( lwork*sizeof(double) );
    /* Solve eigenproblem */
    dsyevx_( &Vchar, &Ichar, &Uchar, &n, laplacian, &lda, &vl, &vu, &il, &iu,&abstol, &m,  eigenvalues, eigenvectors, &ldz, work, &lwork, iwork, ifail, &info );
    /* Check for convergence */
    if( info > 0 ) {
      printf( "The algorithm failed to compute eigenvalues.\n" );
      return(- 1 );
    }

//cerr << endl;
//print_matrix(n,n,laplacian,n);
    /* Print the number of eigenvalues found */
    printf( "\n The total number of eigenvalues found:%2i\n", m );
    /* Print eigenvalues */
    printf( "Selected eigenvalues\n");
    print_matrix(  1.0, m, eigenvalues, 1.0 );
    /* Print eigenvectors */
    //printf(  "Selected eigenvectors (stored columnwise)\n");
    print_matrix( n, m, eigenvectors, m );

    // deallocate
    delete [] laplacian;
    delete [] work;
    delete [] eigenvectors;
  }

  return 0;
}