//compile: g++ mergeBlastGraphs.cpp -o mergeBlastGraphs 
//run: ./mergeBlastGraphs BLASTGRAPH1 BLASTGRAPH2 ....

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>
#include <algorithm>
#include <cmath>

using namespace std;

map<pair<string,string>,map<pair<string,string> ,string> > data;
map<pair<string,string>,string> data_header;
// 	^the 2 files names 		^cur 2 genes 		 ^full line f
//pairs are always lex ordered


// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {

	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

}

pair<string,string> orderedStringPair(string a, string b){if(a<b)return make_pair(a,b);else return make_pair(b,a);}

int main(int argc, char *argv[]) {

	if(argc == 1){
		PRINTHELP:
		cout << "./mergeBlastGraphs BLASTGRAPH1 BLASTGRAPH2 ...." << endl;
		cout << "output is printed to STDOUT. Progress is printed to STDERR." << endl;
	}

	try{

		for(int argi = 1 ; argi < argc ; argi++){

			string line;
			ifstream graph_file(argv[argi]);

			int isfirsttwolines=2;

			if (graph_file.is_open()) {

				pair<string,string> cur_files;

				while (!graph_file.eof()) {

					getline(graph_file, line);

					if(isfirsttwolines>0){

						isfirsttwolines--;

					}else{

						vector<string> fields;
						tokenize(line, fields, "\t");

						if (fields.size() == 2 && fields[0].substr(0, 1) == "#") {
							// Header line
							fields[0] = fields[0].substr(1, fields[0].size() - 1);
							if(fields[0].substr(0, 1) == " "){
								fields[0] = fields[0].substr(1, fields[0].size() - 1);
							}

							cur_files=orderedStringPair(fields[1],fields[0]);

							data_header[cur_files]=line;

						}if(fields.size() == 6){
							
							data[cur_files][orderedStringPair(fields[0],fields[1])] = line;
						
						}
					}
				}
			}
			graph_file.close();

			cerr << "loading " << argv[argi] << " DONE."<< endl;
		}

		cout << "# file_a	file_b\n# a	b	evalue_ab	bitscore_ab	evalue_ba	bitscore_ba"<< endl;
		for(map<pair<string,string>,string>::iterator it = data_header.begin(); it != data_header.end() ; ++it){
			cout << (*it).second << endl;
			for(map<pair<string,string> ,string>::iterator jt = data[(*it).first].begin(); jt != data[(*it).first].end() ; ++jt){
				cout << (*jt).second << endl;
			}
		}

	}catch(string& error) {
		cerr << "[ERROR] catched " << error << endl;

		goto PRINTHELP;
		return EXIT_FAILURE;
	}
	
	return 0;
}
