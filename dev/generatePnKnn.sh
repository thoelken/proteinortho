#!/bin/bash

# Description:
# generate number_of_species/2 times a complete bipartit graph with number_of_species*2 number of nodes
# connect 2 complete bipartite graphs with one edge

number_of_tests=1 # = number of generated graphs
number_of_iterations_per_test=10 #= number of iterations for each graph -> mean runtime value
number_of_species=17
number_of_prot_per_species=33

cur_of_species=2  
while [ $cur_of_species -lt $number_of_species ]
do

	cur_of_prot_per_species=2  
	while [ $cur_of_prot_per_species -lt $number_of_prot_per_species ]
	do

		cur_number_of_tests=0  
		while [ $cur_number_of_tests -lt $number_of_tests ]
		do
			
			#generate random graph

			truncate -s 0 K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav
			echo "# file_a	file_b" > K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav
			echo "# a	b	evalue_ab	bitscore_ab	evalue_ba	bitscore_ba" >> K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav

			i=0
			while [ $i -lt $cur_of_species ]
			do
				j=$(($i+1))

				echo "# $i	$j" >> K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav

				i_pro=0
				while [ $i_pro -lt $cur_of_prot_per_species ]
				do

					j_pro=0
					while [ $j_pro -lt $cur_of_prot_per_species ]
					do

						echo "$i""$i_pro	$j""$j_pro	1	$RANDOM	1	$RANDOM" >> K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav

						j_pro=$(($j_pro+1))
					done
					i_pro=$(($i_pro+1))
				done

				k=$(($i+2))
				if(( $k < $cur_of_species ));then
					m=$(($cur_of_prot_per_species-1))
					echo "# $j	$k" >> K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav
					echo "$j""$m	$k""0	1	$RANDOM	1	$RANDOM" >> K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav
				fi
				
			i=$(($i+2))
			done

				# truncate -s 0 G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav
				# echo "# file_a	file_b" > G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav
				# echo "# a	b	evalue_ab	bitscore_ab	evalue_ba	bitscore_ba" >> G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav

				# i=0
				# while [ $i -lt $cur_of_species ]
				# do
				# 	j=$(($i+1))
				# 	while [ $j -lt $cur_of_species ]
				# 	do

				# 		echo "# $i	$j" >> G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav

				# 		i_pro=0
				# 		while [ $i_pro -lt $cur_of_prot_per_species ]
				# 		do

				# 			j_pro=0
				# 			while [ $j_pro -lt $cur_of_prot_per_species ]
				# 			do

				# 				if(( $RANDOM%$cur_modulo_coef == 0 )); then
				# 					echo "$i""$i_pro	$j""$j_pro	1	$RANDOM	1	$RANDOM" >> G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_number_of_tests.sav
				# 				fi

				# 				j_pro=$(($j_pro+1))
				# 			done
				# 			i_pro=$(($i_pro+1))
				# 		done

				# 	j=$(($j+1))
				# 	done
					
				# i=$(($i+1))
				# done

			cur_number_of_tests=$(($cur_number_of_tests+1))
		done

		cur_of_prot_per_species=$(($cur_of_prot_per_species*2))
	done

	cur_of_species=$(($cur_of_species*2))
done
