#!/bin/bash

# Description:
# 1. generate a random graph 
# 2. use the cluster (fixed) algorithms to obtain a 2 remove.graph (sorting the file and md5sum)
# 3. save the computation time and bool for equal outputs

onlyGraphGen=1

number_of_tests=1 # = number of generated graphs
number_of_iterations_per_test=10 #= number of iterations for each graph -> mean runtime value
number_of_species=17
number_of_prot_per_species=33

number_of_evaule_exponent=20

#1/(modulo_coef) = P(uv \in E(G)) 
number_of_modulo_coef=1


declare -a src=("../openMP_weighted/proteinortho5_clustering.cpp" "../openMP_weighted/proteinortho5_clustering.cpp" "../normal/proteinortho5_clustering.cpp")
declare -a name=("openMP" "openMP_weighted" "normal")
declare -a compileoptions=("-fopenmp" "-fopenmp" "")
declare -a exeoptions=("" "-useWeights 1" "")

arraylength=${#src[@]}
declare -a avg_runtime=( $(for i in {1..${arraylength}}; do echo 0; done) )
declare -a avg_areEqual=( $(for i in {1..${arraylength}}; do echo 0; done) )
declare -a avg_iterations=( $(for i in {1..${arraylength}}; do echo 0; done) )


if (( $onlyGraphGen == 0 )); then
	echo "--- compiling"

	for (( i=1; i<${arraylength}+1; i++ ));
	do
	   DIR=$(dirname "${src[$i-1]}")
	   echo ${src[$i-1]}" -> "$DIR"/"${name[$i-1]}
	   g++ ${compileoptions[$i-1]} ${src[$i-1]} -o $DIR"/"${name[$i-1]}
	done
fi

echo "--- remove LOG"

if [ -f "LOG" ]; then
	rm LOG
fi

echo "--- write header"

HEADER=""

HEADER+="epsilon_exponent,cur_of_species,cur_of_prot_per_species,graph_modulo_coef,graph_id" 

for (( i=1; i<${arraylength}+1; i++ ));
do
	HEADER+=",runtime_"${name[$i-1]}",equal_"${name[$i-1]}",it_"${name[$i-1]} 
done

echo $HEADER > result.txt

echo "--- write row data"

cur_of_species=2  
while [ $cur_of_species -lt $number_of_species ]
do

	cur_of_prot_per_species=2  
	while [ $cur_of_prot_per_species -lt $number_of_prot_per_species ]
	do

		cur_modulo_coef=1
		while [ $cur_modulo_coef -le $number_of_modulo_coef ]
		do

			cur_number_of_tests=0  
			while [ $cur_number_of_tests -lt $number_of_tests ]
			do
				
				#generate random graph

				truncate -s 0 K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav
				echo "# file_a	file_b" > K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav
				echo "# a	b	evalue_ab	bitscore_ab	evalue_ba	bitscore_ba" >> K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav

				i=0
				while [ $i -lt $cur_of_species ]
				do
					j=$(($i+1))

					echo "# $i	$j" >> K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav

					i_pro=0
					while [ $i_pro -lt $cur_of_prot_per_species ]
					do

						j_pro=0
						while [ $j_pro -lt $cur_of_prot_per_species ]
						do

							echo "$i""$i_pro	$j""$j_pro	1	$RANDOM	1	$RANDOM" >> K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav

							j_pro=$(($j_pro+1))
						done
						i_pro=$(($i_pro+1))
					done

					k=$(($i+2))
					if(( $k < $cur_of_species ));then
						m=$(($cur_of_prot_per_species-1))
						echo "# $j	$k" >> K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav
						echo "$j""$m	$k""0	1	$RANDOM	1	$RANDOM" >> K$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav
					fi
					
				i=$(($i+2))
				done

				# truncate -s 0 G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav
				# echo "# file_a	file_b" > G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav
				# echo "# a	b	evalue_ab	bitscore_ab	evalue_ba	bitscore_ba" >> G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav

				# i=0
				# while [ $i -lt $cur_of_species ]
				# do
				# 	j=$(($i+1))
				# 	while [ $j -lt $cur_of_species ]
				# 	do

				# 		echo "# $i	$j" >> G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav

				# 		i_pro=0
				# 		while [ $i_pro -lt $cur_of_prot_per_species ]
				# 		do

				# 			j_pro=0
				# 			while [ $j_pro -lt $cur_of_prot_per_species ]
				# 			do

				# 				if(( $RANDOM%$cur_modulo_coef == 0 )); then
				# 					echo "$i""$i_pro	$j""$j_pro	1	$RANDOM	1	$RANDOM" >> G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav
				# 				fi

				# 				j_pro=$(($j_pro+1))
				# 			done
				# 			i_pro=$(($i_pro+1))
				# 		done

				# 	j=$(($j+1))
				# 	done
					
				# i=$(($i+1))
				# done

				if (( $onlyGraphGen == 0 )); then

					cur_evalue_exponent=1
					while [ $cur_evalue_exponent -le $number_of_evaule_exponent ]
					do

						md5_prev=0

						cur_number_of_iterations_per_test=0
						while [ $cur_number_of_iterations_per_test -lt $number_of_iterations_per_test ]
						do

							areEqual=1

							timestam=`date +%s`

							for (( i=1; i<${arraylength}+1; i++ ));
							do
								if [ -f "remove.graph" ]; then
									rm remove.graph
								fi

	   							DIR=$(dirname "${src[$i-1]}")

	   							echo '' >> LOG
	   							echo ."/"$DIR"/"${name[$i-1]} G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav -epsilon 1e-$cur_evalue_exponent -seed $timestam ${exeoptions[$i-1]} >> LOG

								start=`date +%s%N`
									timeout 11s ."/"$DIR"/"${name[$i-1]} G$cur_of_species"_"$cur_of_prot_per_species"_"$cur_modulo_coef"_"$cur_number_of_tests.sav -epsilon 1e-$cur_evalue_exponent -seed $timestam ${exeoptions[$i-1]} >> LOG
								end=`date +%s%N`
								
								#runtime 
								avg_runtime[$i-1]=$(( ${avg_runtime[$i-1]}+end-start ))

								#areEqual
								sort remove.graph -o remove.graph
								md5=$(md5sum "remove.graph" | cut -d" " -f1)

								if [ "$md5_prev" != "0" ]; then
									if [ "$md5_prev" == "$md5" ]; then
										avg_areEqual[$i-1]=$(( ${avg_areEqual[$i-1]}+1 ))
									fi
								fi
								md5_prev=$md5

								#it
								lastline=$( awk '/./{line=$0} END{print line}' LOG )
								avg_iterations[$i-1]=$(( ${avg_iterations[$i-1]}+$lastline ))

							done

							cur_number_of_iterations_per_test=$(($cur_number_of_iterations_per_test+1))
						done

						for (( i=1; i<${arraylength}+1; i++ ));
						do
							avg_runtime[$i-1]=$(( ${avg_runtime[$i-1]}/$number_of_iterations_per_test ))
						done
						for (( i=1; i<${arraylength}+1; i++ ));
						do
							avg_areEqual[$i-1]=$(( ${avg_areEqual[$i-1]}/($number_of_iterations_per_test-1) ))
						done
						for (( i=1; i<${arraylength}+1; i++ ));
						do
							avg_iterations[$i-1]=$(( ${avg_iterations[$i-1]}/($number_of_iterations_per_test) ))
						done

						curdatarow=$cur_evalue_exponent","$cur_of_species","$cur_of_prot_per_species","$cur_modulo_coef","$cur_number_of_tests

						for (( i=1; i<${arraylength}+1; i++ ));
						do
							curdatarow=$curdatarow","${avg_runtime[$i-1]}","${avg_areEqual[$i-1]}","${avg_iterations[$i-1]}
						done
	 
						echo $curdatarow >> result.txt

						echo $HEADER
						echo $curdatarow
						echo ''

						cur_evalue_exponent=$(($cur_evalue_exponent+1))
					done
				fi

				cur_number_of_tests=$(($cur_number_of_tests+1))
			done

			cur_modulo_coef=$(($cur_modulo_coef+1))
		done

		cur_of_prot_per_species=$(($cur_of_prot_per_species*2))
	done

	cur_of_species=$(($cur_of_species*2))
done

#rm *.sav
rm *.graph
rm LOG