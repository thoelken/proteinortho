#!/bin/bash
#../../hannaBlastGraphs/Hannah.blast-graph_179_180
filename="../../hannaBlastGraphs/Hannah.blast-graph_179_180"
outpath="../../../../public_html/output_double_openmp4"
proteinortho_path=".."

mkdir $outpath

#
# all -O3 -march=native -mtune=native
#

	# nokmere_noomp

		# weighted 

		# compileargs="-O3 -march=native -mtune=native"
		# params="-openmp 4 -kmereHeur 0 -useWeights 1 -seed 12345"
		# name="all_weighted_nokmere_noomp"

		# g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		# echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		# echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		# start=`date +%s%N`
		# 	./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		# end=`date +%s%N`
		# runtime=$(( end-start ))
		# echo $runtime >> $outpath/log_$name.txt

		# # unweighted

		# compileargs="-O3 -march=native -mtune=native"
		# params="-openmp 4 -kmereHeur 0 -useWeights 0 -seed 12345"
		# name="all_nokmere_noomp"

		# g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		# echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		# echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		# start=`date +%s%N`
		# 	./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		# end=`date +%s%N`
		# runtime=$(( end-start ))
		# echo $runtime >> $outpath/log_$name.txt

	#nokmere

		# weighted 

		compileargs="-O3 -march=native -mtune=native -fopenmp"
		params="-openmp 4 -kmereHeur 0 -useWeights 1 -seed 12345"
		name="all_weighted_nokmere"

		g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		start=`date +%s%N`
			./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		end=`date +%s%N`
		runtime=$(( end-start ))
		echo $runtime >> $outpath/log_$name.txt

		# unweighted

		compileargs="-O3 -march=native -mtune=native -fopenmp"
		params="-openmp 4 -kmereHeur 0 -useWeights 0 -seed 12345"
		name="all_nokmere"

		g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		start=`date +%s%N`
			./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		end=`date +%s%N`
		runtime=$(( end-start ))
		echo $runtime >> $outpath/log_$name.txt

	#noomp

		# # weighted 

		# compileargs="-O3 -march=native -mtune=native"
		# params="-openmp 4 -kmereHeur 1 -useWeights 1 -seed 12345"
		# name="all_weighted_noomp"

		# g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		# echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		# echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		# start=`date +%s%N`
		# 	./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		# end=`date +%s%N`
		# runtime=$(( end-start ))
		# echo $runtime >> $outpath/log_$name.txt

		# # unweighted

		# compileargs="-O3 -march=native -mtune=native"
		# params="-openmp 4 -kmereHeur 1 -useWeights 0 -seed 12345"
		# name="all_noomp"

		# g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		# echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		# echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		# start=`date +%s%N`
		# 	./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		# end=`date +%s%N`
		# runtime=$(( end-start ))
		# echo $runtime >> $outpath/log_$name.txt

	#all

		# weighted 

		compileargs="-O3 -march=native -mtune=native -fopenmp"
		params="-openmp 4 -kmereHeur 1 -useWeights 1 -seed 12345"
		name="all_weighted"

		g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		start=`date +%s%N`
			./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		end=`date +%s%N`
		runtime=$(( end-start ))
		echo $runtime >> $outpath/log_$name.txt

		# unweighted

		compileargs="-O3 -march=native -mtune=native -fopenmp"
		params="-openmp 4 -kmereHeur 1 -useWeights 0 -seed 12345"
		name="all"

		g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		start=`date +%s%N`
			./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		end=`date +%s%N`
		runtime=$(( end-start ))
		echo $runtime >> $outpath/log_$name.txt



#
# plain (no opti compiler flags)
#

	# # nokmere_noomp

	# 	# weighted 

	# 	compileargs=""
	# 	params="-openmp 4 -kmereHeur 0 -useWeights 1 -seed 12345"
	# 	name="plain_weighted_nokmere_noomp"

	# 	g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
	# 	echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
	# 	echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
	# 	start=`date +%s%N`
	# 		./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
	# 	end=`date +%s%N`
	# 	runtime=$(( end-start ))
	# 	echo $runtime >> $outpath/log_$name.txt

	# 	# unweighted

	# 	compileargs=""
	# 	params="-openmp 4 -kmereHeur 0 -useWeights 0 -seed 12345"
	# 	name="plain_nokmere_noomp"

	# 	g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
	# 	echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
	# 	echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
	# 	start=`date +%s%N`
	# 		./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
	# 	end=`date +%s%N`
	# 	runtime=$(( end-start ))
	# 	echo $runtime >> $outpath/log_$name.txt

	# #nokmere

	# 	# weighted 

	# 	compileargs="-fopenmp"
	# 	params="-openmp 4 -kmereHeur 0 -useWeights 1 -seed 12345"
	# 	name="plain_weighted_nokmere"

	# 	g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
	# 	echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
	# 	echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
	# 	start=`date +%s%N`
	# 		./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
	# 	end=`date +%s%N`
	# 	runtime=$(( end-start ))
	# 	echo $runtime >> $outpath/log_$name.txt

	# 	# unweighted

	# 	compileargs="-fopenmp"
	# 	params="-openmp 4 -kmereHeur 0 -useWeights 0 -seed 12345"
	# 	name="plain_nokmere"

	# 	g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
	# 	echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
	# 	echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
	# 	start=`date +%s%N`
	# 		./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
	# 	end=`date +%s%N`
	# 	runtime=$(( end-start ))
	# 	echo $runtime >> $outpath/log_$name.txt

	# #noomp

	# 	# weighted 

	# 	compileargs=""
	# 	params="-openmp 4 -kmereHeur 1 -useWeights 1 -seed 12345"
	# 	name="plain_weighted_noomp"

	# 	g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
	# 	echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
	# 	echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
	# 	start=`date +%s%N`
	# 		./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
	# 	end=`date +%s%N`
	# 	runtime=$(( end-start ))
	# 	echo $runtime >> $outpath/log_$name.txt

	# 	# unweighted

	# 	compileargs=""
	# 	params="-openmp 4 -kmereHeur 1 -useWeights 0 -seed 12345"
	# 	name="plain_noomp"

	# 	g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
	# 	echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
	# 	echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
	# 	start=`date +%s%N`
	# 		./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
	# 	end=`date +%s%N`
	# 	runtime=$(( end-start ))
	# 	echo $runtime >> $outpath/log_$name.txt

	# #all

	# 	# weighted 

	# 	compileargs="-fopenmp"
	# 	params="-openmp 4 -kmereHeur 1 -useWeights 1 -seed 12345"
	# 	name="plain_weighted"

	# 	g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
	# 	echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
	# 	echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
	# 	start=`date +%s%N`
	# 		./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
	# 	end=`date +%s%N`
	# 	runtime=$(( end-start ))
	# 	echo $runtime >> $outpath/log_$name.txt

	# 	# unweighted

	# 	compileargs="-fopenmp"
	# 	params="-openmp 4 -kmereHeur 1 -useWeights 0 -seed 12345"
	# 	name="plain"

	# 	g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
	# 	echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
	# 	echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
	# 	start=`date +%s%N`
	# 		./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
	# 	end=`date +%s%N`
	# 	runtime=$(( end-start ))
	# 	echo $runtime >> $outpath/log_$name.txt

#
# only o3 
#


	# compileargs="-O3 -fopenmp"
	# params="-openmp 4 -kmereHeur 1 -useWeights 0 -seed 12345"
	# name="onlyo3"

	# g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
	# echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
	# echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
	# start=`date +%s%N`
	# 	./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
	# end=`date +%s%N`
	# runtime=$(( end-start ))
	# echo $runtime >> $outpath/log_$name.txt


#
# only o2
#

	# compileargs="-O2 -fopenmp"
	# params="-openmp 4 -kmereHeur 1 -useWeights 0 -seed 12345"
	# name="onlyo2"

	# g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
	# echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
	# echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
	# start=`date +%s%N`
	# 	./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
	# end=`date +%s%N`
	# runtime=$(( end-start ))
	# echo $runtime >> $outpath/log_$name.txt

filename="../../hannaBlastGraphs/Hannah.blast-graph_179_180"
outpath="../../../../public_html/output_double_openmp8"
proteinortho_path=".."

mkdir $outpath

#
# all -O3 -march=native -mtune=native
#
	#no kmere
		# weighted 

		compileargs="-O3 -march=native -mtune=native -fopenmp"
		params="-openmp 8 -kmereHeur 0 -useWeights 1 -seed 12345"
		name="all_weighted_nokmere"

		g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		start=`date +%s%N`
			./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		end=`date +%s%N`
		runtime=$(( end-start ))
		echo $runtime >> $outpath/log_$name.txt

		# unweighted

		compileargs="-O3 -march=native -mtune=native -fopenmp"
		params="-openmp 8 -kmereHeur 0 -useWeights 0 -seed 12345"
		name="all_nokmere"

		g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		start=`date +%s%N`
			./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		end=`date +%s%N`
		runtime=$(( end-start ))
		echo $runtime >> $outpath/log_$name.txt

	#all

		# weighted 

		compileargs="-O3 -march=native -mtune=native -fopenmp"
		params="-openmp 8 -kmereHeur 1 -useWeights 1 -seed 12345"
		name="all_weighted"

		g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		start=`date +%s%N`
			./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		end=`date +%s%N`
		runtime=$(( end-start ))
		echo $runtime >> $outpath/log_$name.txt

		# unweighted

		compileargs="-O3 -march=native -mtune=native -fopenmp"
		params="-openmp 8 -kmereHeur 1 -useWeights 0 -seed 12345"
		name="all"

		g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static
		echo "g++ $compileargs $proteinortho_path/proteinortho5_clustering.cpp -o proteinortho5_clustering -static" >> $outpath/log_$name.txt
		echo "./proteinortho5_clustering $params $filename" >> $outpath/log_$name.txt
		start=`date +%s%N`
			./proteinortho5_clustering $params $filename >> $outpath/log_$name.txt
		end=`date +%s%N`
		runtime=$(( end-start ))
		echo $runtime >> $outpath/log_$name.txt
