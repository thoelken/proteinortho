//compile: g++ blastgraph2ivenn.cpp -o blastgraph2ivenn 
//run: ./blastgraph2ivenn BLASTGRAPH1 BLASTGRAPH2 ... 
// and then upload to http://www.interactivenn.net/ 
//pk
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <string.h>

using namespace std;

// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {

	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

}

string orderedStringPairID(string a, string b){if(a<b)return a+"-"+b;else return b+"-"+a;}

map<string,unsigned int> reduce;
unsigned int c=0;



int main(int argc, char *argv[]) {

	string id_a="",id_b="";

	if(argc < 2 || argv[1] == "-h" || argv[1] == "--h" || argv[1] == "-help" || argv[1] == "--help" || argv[1] == "help" || argv[1] == "h"){
		PRINTHELP:
		cerr << "./blastgraph2ivenn BLASTGRAPH1 BLASTGRAPH2 ...>output.ivenn" << endl;
		cerr << "Output is written to STDOUT. -> upload to http://www.interactivenn.net/ " << endl;
		return -1;
	}else if(strlen(argv[1])==0 || argv[1][0] == '-'){
		cout << "[ERROR] : unknown option" << endl;
		goto PRINTHELP;
	}

	try {
		for(unsigned int argi = 1 ; argi < argc ; argi++){
			if(strlen(argv[argi])==0 || argv[argi][0] == '-'){continue;}

			cout << argv[argi] << ":"; 

			int uniquehits=0,notuniquehits=0;

			string line;
			ifstream graph_file(argv[argi]);
			bool first=true;

			if (graph_file.is_open()) {
				while (!graph_file.eof()) {
					getline(graph_file, line);

					vector<string> fields;
					tokenize(line, fields, "\t");

					if (line.length()>1 && line.substr(0, 1) == "#"&& fields.size()==2) {
						id_a=fields[0].substr(2, fields[0].size()-2);
						id_b=fields[1];
					}
					if (line.length()>1 && line.substr(0, 1) != "#") {

						if(fields.size()<2)
							continue;

						if(first){
							first=!first;
						}else{
							cout << ",";
						}

						//string cur_id = orderedStringPairID(id_a+fields[0],id_b+fields[1]);
						string cur_id = id_a+fields[0]+"-"+id_b+fields[1];

						if(reduce.count(cur_id)){
							cout << reduce[cur_id] ;
							notuniquehits++;
							//cerr << cur_id << endl;
						}else{
							c++;
							reduce[cur_id]=c;
							cout << c ;
							uniquehits++;
						}
					}
				}
				cout << ";"<< endl;
				cerr << argv[argi] << " uniquehits=" << uniquehits << " hits in common with previous sets="<<notuniquehits << endl; 
			}
		}
		
	}catch(string& error) {
		cerr << "[ERROR] catched " << error << endl;
		goto PRINTHELP;
		return EXIT_FAILURE;
	}
	return 0;
}
