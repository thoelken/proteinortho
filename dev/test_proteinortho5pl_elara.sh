#!/bin/bash

mkdir blastp+

start=`date +%s%N`
perl proteinortho5.pl -project=time_blastp+ -step=1 -p=blastp+ -tmp=blastp+ -keep $(cat list)
perl proteinortho5.pl -project=time_blastp+ -step=2 -p=blastp+ -tmp=blastp+ -keep $(cat list)
end=`date +%s%N`
runtime=$(( end-start ))
echo "blastp+	"$runtime >> time_log.txt

mkdir diamond

start=`date +%s%N`
perl proteinortho5.pl -project=time_diamond -step=1 -p=diamond -tmp=diamond -keep $(cat list)
perl proteinortho5.pl -project=time_diamond -step=2 -p=diamond -tmp=diamond -keep $(cat list)
end=`date +%s%N`
runtime=$(( end-start ))
echo "diamond	"$runtime >> time_log.txt

mkdir diamond-moresensitive

start=`date +%s%N`
perl proteinortho5.pl -project=time_diamond-moresensitive -step=1 -tmp=diamond-moresensitive -p=diamond-moresensitive -keep $(cat list)
perl proteinortho5.pl -project=time_diamond-moresensitive -step=2 -tmp=diamond-moresensitive -p=diamond-moresensitive -keep $(cat list)
end=`date +%s%N`
runtime=$(( end-start ))
echo "diamond-moresensitive	"$runtime >> time_log.txt

mkdir phmmer

start=`date +%s%N`
perl proteinortho5.pl -project=time_phmmer -step=1 -p=phmmer -tmp=phmmer -keep $(cat list)
perl proteinortho5.pl -project=time_phmmer -step=2 -p=phmmer -tmp=phmmer -keep $(cat list)
end=`date +%s%N`
runtime=$(( end-start ))
echo "phmmer	"$runtime >> time_log.txt

# mkdir jackhmmer

# start=`date +%s%N`
# perl proteinortho5.pl -project=time_jackhmmer -step=1 -p=jackhmmer -tmp=jackhmmer -keep $(cat list)
# perl proteinortho5.pl -project=time_jackhmmer -step=2 -p=jackhmmer -tmp=jackhmmer -keep $(cat list)
# end=`date +%s%N`
# runtime=$(( end-start ))
# echo "jackhmmer	"$runtime >> time_log.txt

mkdir lastp

start=`date +%s%N`
perl proteinortho5.pl -project=time_lastp -step=1 -p=lastp -tmp=lastp -keep $(cat list)
perl proteinortho5.pl -project=time_lastp -step=2 -p=lastp -tmp=lastp -keep $(cat list)
end=`date +%s%N`
runtime=$(( end-start ))
echo "lastp	"$runtime >> time_log.txt

mkdir rapsearch

start=`date +%s%N`
perl proteinortho5.pl -project=time_rapsearch -step=1 -p=rapsearch -tmp=rapsearch -keep $(cat list)
perl proteinortho5.pl -project=time_rapsearch -step=2 -p=rapsearch -tmp=rapsearch -keep $(cat list)
end=`date +%s%N`
runtime=$(( end-start ))
echo "rapsearch	"$runtime >> time_log.txt

mkdir usearch

start=`date +%s%N`
perl proteinortho5.pl -project=time_usearch -step=1 -p=usearch -tmp=usearch -keep $(cat list)
perl proteinortho5.pl -project=time_usearch -step=2 -p=usearch -tmp=usearch -keep $(cat list)
end=`date +%s%N`
runtime=$(( end-start ))
echo "usearch	"$runtime >> time_log.txt

mkdir usearch-ublast

start=`date +%s%N`
perl proteinortho5.pl -project=time_usearch-ublast -step=1 -p=usearch-ublast -tmp=usearch-ublast -keep $(cat list)
perl proteinortho5.pl -project=time_usearch-ublast -step=2 -p=usearch-ublast -tmp=usearch-ublast -keep $(cat list)
end=`date +%s%N`
runtime=$(( end-start ))
echo "usearch-ublast	"$runtime >> time_log.txt

sort time_log.txt -k2,2g >asd.txt
mv asd.txt time_log.txt